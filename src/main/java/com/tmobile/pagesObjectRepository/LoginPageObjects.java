package com.tmobile.pagesObjectRepository;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPageObjects {
	
	WebDriver driver;
    Hashtable<String, String> dataTable;

    public By OktaSignInUserName = By.id("okta-signin-username");
    public By OktaSignInPassword = By.name("password");
    public By OktaSignInSubmit = By.id("okta-signin-submit");
    public By OktaSignInVerify = By.cssSelector("input.button.button-primary");
    public By SignInRole = By.id("role-dropdown");
    public By SignInRoleItem = By.xpath("//li[@id='roleList']/div");
//    public By SignInRoleItem = By.xpath("(//li[@id='roleList']/div)[5]");
    public By ClickContinueButton = By.xpath("//button");

}
