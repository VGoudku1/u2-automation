package com.tmobile.pagesObjectRepository;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BringYourDevicePageObjects {
	WebDriver driver;
    Hashtable<String, String> dataTable;
    
    public By EnterZipCode = By.cssSelector("[id='zip'] input");
    public By zipContinueButton = By.id("entry-modal-continue-cta");
    public By EnterIEMEITextbox = By.cssSelector("tmo-device-validation input");
    public By CheckCompatibilityButton = By.id("checkCompatibility");
    public By DeviceValidationSpinner = By.cssSelector("tmo-device-validation tmo-spinner i.spinner-icon");
    public By ValidIEMEIText = By.cssSelector("div.ui-dialog-content  h3[style]");
    public By CloseModalButton = By.cssSelector(".ui-dialog-content tmo-button button");
    public By UnlockedPhoneCheckBox = By.cssSelector("tmo-checkbox input ~ span");
    public By EnterSimNumberTextBox = By.cssSelector("[formcontrolname='haveSim'] input");
    public By EntereSIMNumberTextBox = By.cssSelector("[formcontrolname='haveEsim'] input");
    public By SelectUseMyeSIMCard = By.xpath("//p[contains(text(),'eSIM')]//parent::div//button[@id='submitSimButton']");
    public By SelectUseMySimCard = By.xpath("//p[contains(text(),'Use my')]//parent::div//button[@id='submitSimButton']");
    public By SelectSimCardYouNeed = By.xpath("//p[contains(text(),'SIM you need')]//parent::div//button[@id='submitSimButton']");
    public By ChoosePlan = By.id("tmo-button-continue-button");
    public By PortPrepaidContinueButton = By.xpath("//p[text()=' Lines must be ported one at a time.']//ancestor::div//button[@id='continue-modal']");
    
    //tmo-selected-product-details tmo-spinner i.spinner-icon
    //tmo-spinner[tmo-spinner]

}
