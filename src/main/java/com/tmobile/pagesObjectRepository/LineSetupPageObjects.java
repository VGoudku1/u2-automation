package com.tmobile.pagesObjectRepository;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LineSetupPageObjects {
	WebDriver driver;
    Hashtable<String, String> dataTable;
  
    public By GetNewNumberRadioButtonXpath = By.xpath("//tmo-radio-button//p[text()= ' Get a new number ']//ancestor::label//span");
    public By GetNewNumberRadioButton = By.cssSelector("[id='get-new-number-radio-btn-0'] ~ span");
    public By EnterAreaCodeInput = By.cssSelector("[formcontrolname='zipAreaCodePort'] input");
    public By SearchAreaCode = By.cssSelector("[formcontrolname='zipAreaCodePort'] ~ a");
    public By InlineSpinner = By.xpath("//tmo-inline-spinner/div[not(@hidden)]//i");
    
    //[//tmo-inline-spinner/div[not(@hidden)]//i
    //tmo-spinner[tmo-spinner]

}
