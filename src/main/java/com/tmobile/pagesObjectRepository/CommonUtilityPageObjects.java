package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class CommonUtilityPageObjects {
	
public By SetOrderDateButton = By.xpath("//tmo-change-plan-services//button[contains(text(),'Set order date')]");
	
	public By buttonWithAriaLabel(String ariaLabel) {
		return By.cssSelector("button[aria-label='"+ariaLabel+"']");
	}

}
