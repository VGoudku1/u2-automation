package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class IMEICheckPageObjects {
	
	public By EnterIMEITextBox = By.cssSelector("[formcontrolname='imei'] input");
	public By SearchIMEIButton = By.cssSelector("[id='searchButton']");
	public By SelectBlockReason = By.cssSelector("tmo-dropdown[placeholder='Block reason']");
	public By AddBlockButton = By.cssSelector("button[name='Add Block']");
			

}
