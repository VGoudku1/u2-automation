package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class LineLandingPageObjects {
	
	
	public By ReportLostStolenLink = By.cssSelector("[id='report-lost-stolen']");
	public By ExpandRepActions = By.cssSelector("[id='rep-actions'] a h6");
	public By ExpandLineDevice = By.cssSelector("[id='line-device'] a h6");
	public By ExpandData = By.cssSelector("tmo-session-pass a h6");
	public By ChangePlanServicesButton = By.cssSelector("button[id='Edit-plan-and-services']");
	public By OnDemandPassButton = By.cssSelector("button[id='add-session-passes']");
	public By OnDemandPassQuantuty = By.cssSelector("div[id*='quantity'] tmo-dropdown");
	public By SetOrderDateButton = By.xpath("//tmo-change-plan-services//button[contains(text(),'Set order date')]");
	
	//div[id*="quantity"] tmo-dropdown
	public By lineDetailsOption(String optionName) {
		return By.xpath("//*[@id='line-details-buttons']//a//span[contains(text(),'"+optionName+"')]");
	}
	
	public By selectDemandPass(String passName) {
		return By.xpath("//tmo-radio-button//p[contains(text(),'"+passName+"')]");
	}
	
	


}
