package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class SupportLandingPageObjects {
	
	public By SupportLinks = By.cssSelector("a[id='support-links'] h6");
	public By DeviceOptiosHeader = By.cssSelector("tmo-accordion-blade div.ui-accordion-header h6");
	public By UnblockSubOptions = By.cssSelector("a[href*='/care/'] h6");
	public By ClickRemoveButton = By.cssSelector("a[id='removeImeiBlock']");

}
