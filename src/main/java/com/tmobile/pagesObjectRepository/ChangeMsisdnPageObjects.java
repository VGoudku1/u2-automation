package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class ChangeMsisdnPageObjects {
	
	public By GetANewNumberButton = By.cssSelector("tmo-change-msisdn button.btn.btn-primary");
	public By EnterZiporAreaCode = By.cssSelector("[formcontrolname='zipOrAreaCode'] input");
	public By SearchButton = By.cssSelector(".search-box-go button");
	public By GetThisNumberButton = By.cssSelector("tmo-new-number button.btn-primary");
	public By SubmitRequestButton = By.cssSelector("div.ui-dialog-footer.ui-widget-content button.btn-primary");

}
