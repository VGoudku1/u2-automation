package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class OrderSummaryPageObjects {
	
	
	public By OrderSummaryPageTitle = By.cssSelector("div.page-title h2");
	public By ClickMyTMobileLink = By.cssSelector("li[role='menuitem'] a");
	public By MyTmobileSubMenu = By.cssSelector("span.mytmo-submenu ul li.submenu-item a");

}
