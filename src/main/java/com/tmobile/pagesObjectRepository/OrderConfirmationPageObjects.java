package com.tmobile.pagesObjectRepository;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OrderConfirmationPageObjects {

	
	WebDriver driver;
    Hashtable<String, String> dataTable;
    
    public By OrderConfrimPageTitle = By.cssSelector("tmo-page-titles h1");
    public By SuspendSuccessTitle = By.cssSelector(".page-title h1");
    public By OrderNumber = By.cssSelector("#order-number-text");
    public By SuspendOrderNumber = By.cssSelector("tmo-confirmation-lost-stolen p.lead.bold");
    public By CancelOrderNumber = By.cssSelector("tmo-confirmation-cancel-line-landing div.m-t-xl p.lead.bold");
    public By ResumeOrderNumber = By.cssSelector("tmo-confirmation-cancel-line-landing div.m-t-xl p.lead.bold");
    public By ChangeSimOrderNumber = By.cssSelector("tmo-sim-confirmation p.lead.m-t-xl.bold");
    public By BlockIMEIOrderNumber = By.cssSelector("tmo-imei-check-confirmation p.lead.m-t-lg");
}
