package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class ShippingPageObjects {
	
	public By FirstName = By.cssSelector("[formcontrolname='firstName']");
	public By LastName = By.cssSelector("[formcontrolname='lastName']");
	public By Address1 = By.cssSelector("[formcontrolname='address1'] input");
    public By EnterZipCode = By.cssSelector("[formcontrolname='zipCode'] input");
    public By EnterCity = By.cssSelector("[formcontrolname='city'] input");
    //go-to-cart

}
