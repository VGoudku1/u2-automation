package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class CancelLinePageObjects {

	public By SelectTheLineCheckbox = By.cssSelector("tmo-checkbox span span");
	public By CancelReasonDropdown = By.cssSelector("tmo-dropdown[formcontrolname='cancellationReason']");
	public By CancelSuccess = By.cssSelector("tmo-dropdown[formcontrolname='cancellationReason']");
	public By CancelOrder = By.cssSelector("tmo-dropdown[formcontrolname='cancellationReason']");
	public By CancelContinueButton = By.cssSelector("button[name='Next']");
	//button[name='Next']
	
}
