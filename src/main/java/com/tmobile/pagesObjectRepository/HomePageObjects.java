package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Hashtable;

public class HomePageObjects {
    WebDriver driver;
    Hashtable<String, String> dataTable;

    public By ClickNavigationMenuItem = By.cssSelector("ul.links-menu a span");
    public By ClickNavigationSubMenuItem = By.cssSelector("ul.small.submenu li.full a");

    public By SignInRole = By.id("role-dropdown");
    public By SignInRoleItem = By.xpath("(//li[@id='roleList']/div)[5]");
    public By ClickContinueButton = By.cssSelector("button.btn.btn-primary");
    public By NavBarLookup = By.cssSelector("a[aria-label='Lookup']");
    public By NavBarAccounts = By.cssSelector("a[id='accountsTab']");
    public By OrderLookupLink = By.cssSelector("a[id='cta-order-lookup']");
    public By OrderNumberInput = By.cssSelector("input[id='orderId']");
    public By OrderSearchButton = By.cssSelector("button[id='searchOrdersBtn']");
    public By ClickOrderResult = By.cssSelector("#manual-search-results p");
    public By ClickByPassOption = By.cssSelector("a[id='care-bypass-link']");
    public By VerificationPageSpinner = By.xpath("//div[@id='show-spinner-order-verification']/div[not(@hidden)]//i");
    ////div[@id='show-spinner-order-verification']/div[not(@hidden)]//i
    



}
