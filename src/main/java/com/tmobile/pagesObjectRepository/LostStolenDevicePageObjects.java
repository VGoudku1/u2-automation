package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

public class LostStolenDevicePageObjects {

	
	public By YesSuspendRadioButton = By.cssSelector("[formcontrolname='suspendLineRadio'] span");
	public By YesBlockDeviceRadioButton = By.cssSelector("[formcontrolname='blockUnblockImeiRadio'] span");
	public By AcceptTncCheckbox = By.cssSelector("[formcontrolname='termsAndConditions'] span");
	public By ClickSubmitButton = By.cssSelector("tmo-repdash button.btn.btn-primary");
	public By ClickBlockIMEIButton = By.cssSelector("tmo-report-device-questions button");
	
}
