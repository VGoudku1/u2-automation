package com.tmobile.pagesObjectRepository;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountSetUpPageObjects {
	WebDriver driver;
    Hashtable<String, String> dataTable;
    
    public By SelectGetNewNumber = By.cssSelector("#get-new-number-radio-btn-0");
    public By EnterZipCode = By.cssSelector("#new-number-zip-areaCode-input0");
    public By ClickContinueWithAccountButton = By.id("continue-btn");
    
    public By ClickSearch = By.cssSelector("#cta-default-116");
    public By ReserveNumber = By.cssSelector("#reserve");
    public By ClickContinue = By.cssSelector("#backToTop");
    
    
    public By SelectSimCard = By.cssSelector("div.simReuse-box-footer button[id='submitSimButton']");
    public By ChoosePlan = By.id("tmo-button-continue-button");
    
    //tmo-selected-product-details tmo-spinner i.spinner-icon
    //tmo-spinner[tmo-spinner]

}
