package com.tmobile.pagesObjectRepository;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaymentPageObjects {
	WebDriver driver;
    Hashtable<String, String> dataTable;
    
    public By SelectNewCreditCard = By.xpath("//*[@id=\"card-entryform-payment\"]/div[2]/div/tmo-stored-payment-dropdown/tmo-dropdown/p-dropdown/div/label");
    public By EditPayment = By.cssSelector("#edit-payment");    
    public By EnterName = By.cssSelector("[formcontrolname='fullName'] input");
    public By EnterCardNumber = By.cssSelector("[formcontrolname='cardNumber'] input");
    public By ExpirationMonth = By.cssSelector("[formcontrolname='expirationMonth']");
    public By ExpirationMonthList = By.cssSelector("[formcontrolname='expirationMonth'] li span");
    public By ExpirationYear = By.cssSelector("[formcontrolname='expirationYear']");
    public By ExpirationYearList = By.cssSelector("[formcontrolname='expirationYear'] li span");
    public By EnterCvv = By.cssSelector("[formcontrolname='cvv'] input");
    public By ClickDoneButton = By.cssSelector("#done-payment");
    
    public By AddressLine1 = By.cssSelector("[formcontrolname='address1'] input");
    public By EnterZipCode = By.cssSelector("[formcontrolname='zipCode'] input");
    public By EnterCity = By.cssSelector("[formcontrolname='city'] input");
    public By SlectContinue = By.cssSelector("#address-validator-select-button");
    public By CheckTnC = By.cssSelector("[id='cartPaymentTC'] tmo-checkbox span.checkbox");
    public By ClickContinue = By.cssSelector("#button-on-continue");
    
    
    
    //tmo-selected-product-details tmo-spinner i.spinner-icon
    //expirationYear

}
