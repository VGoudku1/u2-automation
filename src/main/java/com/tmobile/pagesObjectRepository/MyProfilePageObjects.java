package com.tmobile.pagesObjectRepository;

import org.openqa.selenium.By;

import com.tmobile.commons.GenericKeywords;

public class MyProfilePageObjects extends GenericKeywords {

	public By MyProfileHeader = By.cssSelector("tmo-manage-profile a[href*='my-profile'] h6");

	public By editProfileInformation(String editInformation) {
		By editInfo = By.xpath("//tmo-profile-information//h3[text()='" + editInformation + "']//parent::div//p//a");
		return editInfo;

	}
	public By SaveProfileInfoButton = By.cssSelector("tmo-profile-information button[class='btn btn-primary']");
	public By ChangesSaved = By.xpath("//tmo-profile-information//p//i//parent::p");
	public By AccountEmailTextBox = By.cssSelector("[formcontrolname='email'] input");
	public By RetypeAccountEmailTextBox = By.cssSelector("[formcontrolname='retypeEmail'] input");

}
