package com.tmobile.pagesObjectRepository;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.tmobile.commons.GenericKeywords;

public class ProductSelectorPageObjects extends GenericKeywords {
	
	WebDriver driver;
    Hashtable<String, String> dataTable;
    private String buttonText = null;
    public By ConectTMobilePlanHeader = By.xpath("//p-header//h6[contains(text(),'Connect by T-Mobile')]//ancestor::a");
    public By PlanHeader = By.cssSelector("p-header h6");
    public By VisibleSpinner = By.xpath("//tmo-spinner/div[not(@hidden)]//i");
    public By Dollar_10_Plan = By.xpath("//input[@id='SO_TMC_1000TT_PRE']//parent::span//span");
    public By Dollar_15_Plan = By.xpath("//input[@id='SO_TMC_2GB_PRE']//parent::span//span");
    public By Dollar_25_Plan = By.id("//input[@id='SO_TMC_5GB_PRE']//parent::span");
    public By Dollar_35_Plan = By.id("//input[@id='SO_TMC_12GB_PRE']//parent::span//span");
    public By GoTOCartButton = By.id("go-to-cart");
    public By CheckOutButton = By.id("checkoutCartButton");
    public By ClickTmoButtonWithText = By.xpath("//tmo-button//button[text()='"+buttonText+"']");
    public By EnterPIN = By.cssSelector("[formcontrolname='pin'] input");
    public By ConfrimPIN = By.cssSelector("[formcontrolname='confirmPin'] input");
    public By FirstName = By.cssSelector("[formcontrolname='firstName'] input");
    public By LastName = By.cssSelector("[formcontrolname='lastName'] input");
    public By AddressLine1 = By.cssSelector("[formcontrolname='addressLine1'] input");
    public By AddressLine2 = By.cssSelector("[formcontrolname='addressLine2'] input");
    public By SuggestedAddress = By.xpath("//strong[contains(text(),'Suggested ')]//parent::p//following-sibling::div//tmo-radio-button//span");
    public By AddressValidatorCountinue = By.id("address-validator-select-button");
    public By PlanChangePopup = By.cssSelector("tmo-popup .popup-info");
    //go-to-cart
    
    

}
