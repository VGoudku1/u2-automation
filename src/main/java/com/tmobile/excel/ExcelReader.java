package com.tmobile.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader{
	public static XSSFWorkbook book;
	public static XSSFSheet sheet;
	FileInputStream file = null;

	/**
	 * Custom constructor with file location while object instantiation 
	 * @param path
	 * @author Sangeetha
	 * @updated Nataraaj
	 */
	public ExcelReader(String path) {
		try {
			file = new FileInputStream(path);
			book = new XSSFWorkbook(file);
		}catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}

	/**
	 * Function to get total row count of specific sheet
	 * @param sheetName
	 * @return Number of row(s)
	 */
	public int getRowCount(String sheetName) {
		return getSheetFromExcel(sheetName).getLastRowNum();
	}

	/**
	 * Function to specified row number
	 * @param sheetName
	 * @param rowNumber
	 * @return
	 */
	public XSSFRow getRowFromExcel(XSSFSheet sheet, int rowNumber){
		return sheet.getRow(rowNumber);
	}

	/**
	 * Function to specified row number
	 * @param sheetName
	 * @param rowNumber
	 * @return
	 */
	public XSSFRow getRowFromExcel(String sheetName, int rowNumber){
		return getRowFromExcel(getSheetFromExcel(sheetName), rowNumber);
	}

	/**
	 * Function to get specified sheet from an excel
	 * @param sheetName
	 * @return
	 */
	public XSSFSheet getSheetFromExcel(String sheetName){
		return book.getSheet(sheetName);
	}

	/**
	 * Function to get the Header name and its cell location
	 * @param sheetName
	 */
	public HashMap<String, Integer> getColumnHeaderNameAndCellNumber(String sheetName){
		HashMap<String, Integer> columnNameAndLocation = new HashMap<>();
		try{
			sheet = getSheetFromExcel(sheetName);
			XSSFRow row = getRowFromExcel(sheet, 0);
			for (int i = 0; i <= getColumnCount(sheetName); i++) {
				try{
					String cellValue = row.getCell(i).getStringCellValue();
					if(!cellValue.isEmpty())
						columnNameAndLocation.put(cellValue, i);
				}catch(NullPointerException n){}
			}
		}catch(Exception e){
			System.out.println(e.getStackTrace());
		}
		return columnNameAndLocation;
	}

	/**
	 * Function to get cell value from specified sheet, base on column name and row number
	 * @param sheetName
	 * @param ColumnName
	 * @param rowNumber
	 * @return Cell value
	 */
	public String getCellData(String sheetName, String ColumnName, int rowNumber) {
		return getRowFromExcel(sheetName, rowNumber).getCell(getColumnNumber(sheetName, ColumnName)).toString().trim();
	}

	/**
	 * Function to set cell data for the specified cell number
	 * @param dataToInsert
	 * @param sheetName
	 * @param rowNumber
	 * @param columnNumber
	 */
	public void setCellData(String dataToInsert, String sheetName, int rowNumber, int columnNumber) throws IOException{
		getRowFromExcel(sheetName, rowNumber).createCell(columnNumber).setCellValue(dataToInsert);
//		getRowFromExcel(sheetName, rowNumber).getCell(columnNumber).setCellValue(dataToInsert);
	}

	/**
	 * Function to write data to the specified excel sheet
	 * @param path
	 */
	public void writeDataToExcel(String path){
		try {
			FileOutputStream fos = new FileOutputStream(new File(path));
			book.write(fos);
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Function to get cell value from specified sheet, base on column number and row number
	 * @param sheetName
	 * @param ColumnNumber
	 * @param rowNumber
	 * @return Cell value
	 */
	public String getCellData(String sheetName, int ColumnNumber, int rowNumber) {
		return getRowFromExcel(sheetName, rowNumber).getCell(ColumnNumber).toString().trim();
	}

	/**
	 * Function to get total column count of specific row
	 * @param sheetName
	 * @return
	 */
	public int getColumnCount(String sheetName) {	
		return getRowFromExcel(sheetName, 0).getLastCellNum();
	}

	/**
	 * Function to get the column number of the Specified column name
	 * @param sheetName
	 * @param ColumnName
	 * @return
	 */
	public int getColumnNumber(String sheetName, String ColumnName){
		int colNumber=-1;
		try{
			sheet = getSheetFromExcel(sheetName);
			for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
				if (sheet.getRow(0).getCell(i).toString().trim().equals(ColumnName)){
					colNumber = i;
					break;
				}
			}
			if(colNumber==-1)
				throw new Exception();
		}catch(Exception e){
			System.out.println(e.getStackTrace());
		}
		return colNumber;
	}

	/**
	 * Function to get list of features to execute
	 * @param sheet
	 * @return
	 * @author Nataraaj
	 */
	public ArrayList<String> featureToExecute(String sheetName){
		XSSFRow row;
		ArrayList<String> featurestoExecute= new ArrayList<>();
		try{
			sheet = getSheetFromExcel(sheetName);
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				try{
					row=sheet.getRow(i);
					if(row.getCell(1).toString().equalsIgnoreCase("Y"))
						featurestoExecute.add(sheet.getRow(i).getCell(0).toString().trim());
				}catch(NullPointerException n){}
			}
		}catch(Exception e){
			System.out.println(e.getStackTrace());
		}
		return featurestoExecute;
	}

	/**
	 * Function to fetch below details from datasheet with, features and scenario(s) to Execute and testData for those scenario(s)
	 * @param SheetName
	 * @return Testdata
	 * @author Nataraaj
	 */
	public HashMap<String, ArrayList<HashMap<String, String>>> masterExecutionData(String sheetName){
		HashMap<String, ArrayList<HashMap<String, String>>> allExecutingScenariosTestData= new LinkedHashMap<>();
		try{
			XSSFRow row;

			//get list of features to be executed
			ArrayList<String> featurestoExecute=featureToExecute(sheetName);

			//Testdata collection based on feature list
			for (String featureName : featurestoExecute) {
				try{
					sheet = book.getSheet(featureName);
					int columnCount=sheet.getRow(0).getLastCellNum();
					for (int i = 1; i <= sheet.getLastRowNum(); i++) {
						ArrayList<HashMap<String, String>> tempList= new ArrayList<>();
						HashMap<String,String> singleDataSet = new LinkedHashMap<String,String>();
						row=sheet.getRow(i);
						//Dataset from the specific sheet					
						if(row.getCell(1).toString().equalsIgnoreCase("Y")){
							singleDataSet.put("rowNumber",""+i);
							singleDataSet.put("featureName",featureName);
							for (int j = 0; j < columnCount; j++) {
								try{
									XSSFCell cell = row.getCell(j);
									cell.setCellType(CellType.STRING);
									singleDataSet.put(sheet.getRow(0).getCell(j).toString().trim(), cell.toString().trim());
								}catch(NullPointerException n){}
							}
							if(allExecutingScenariosTestData.containsKey(singleDataSet.get("Scenario"))){
								tempList=allExecutingScenariosTestData.get(singleDataSet.get("Scenario"));
							}
							tempList.add(singleDataSet);
							allExecutingScenariosTestData.put(singleDataSet.get("Scenario"),tempList);
						}
					}
				}catch(NullPointerException n){}
			}
		}catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("Running All Scenario: "+allExecutingScenariosTestData.keySet());
		return allExecutingScenariosTestData;
	}
}
