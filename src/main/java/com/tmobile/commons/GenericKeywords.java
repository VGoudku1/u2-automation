package com.tmobile.commons;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.github.javafaker.Faker;
import com.tmobile.pagesObjectRepository.BringYourDevicePageObjects;
import com.tmobile.pagesObjectRepository.HomePageObjects;
import com.tmobile.pagesObjectRepository.LoginPageObjects;
import com.tmobile.utils.PasswordProtect;

public class GenericKeywords {

	public static Properties suiteProp = new Properties();
	protected static HashSet<String> failedScenario = new HashSet<>();
	protected static boolean rerun = false;

	protected static String userLoggedInName = null;
	protected static String appurl = null;
	protected static String counter = null;	

	/** Thread local entities */
	public static ThreadLocal<WebDriver> threadDriver = new ThreadLocal<>();
	protected static ThreadLocal<ExtentTest> threadExtentReporter = new ThreadLocal<>();
	public static ThreadLocal<HashMap<String, String>> threadTestData = new ThreadLocal<>();
	protected static ThreadLocal<HashMap<String, String>> threadValidateData = new ThreadLocal<>();

	/**
	 * Function to return Test Data for the current thread
	 * 
	 * @return
	 */
	protected HashMap<String, String> getTestData() {
		return threadTestData.get();
	}

	/**
	 * Function to return runtime dynamic validation Data for the current thread
	 * 
	 * @return
	 */
	protected HashMap<String, String> getValidateData() {
		return threadValidateData.get();
	}

	/**
	 * Function to return WebDriver object for the current thread
	 * 
	 * @return
	 */
	protected WebDriver getDriver() {
		return threadDriver.get();
	}

	/**
	 * Function to return reporter object for the current thread
	 * 
	 * @return
	 */
	protected ExtentTest getReporter() {
		return threadExtentReporter.get();
	}

	/**
	 * Initialization of property file location
	 * 
	 * @updated Vikram
	 */
	static {
		try {
			InputStream input = new FileInputStream("src/test/resources/application.properties");
			suiteProp.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Function to fetch method name of the function, from where this is invoked
	 * 
	 * @return MethodName
	 * @author Vikram
	 */
	protected String getMethodName() {
		return new Throwable().getStackTrace()[1].getMethodName();
	}

	/**
	 * Function to get value from a element
	 * 
	 * @return text
	 * @updated Vikram
	 */
	protected String getText(By webElement) {
		return getText(getElement(webElement));
	}

	/**
	 * Function to get value from a webelement
	 * 
	 * @return text
	 * @author Vikram
	 */
	protected String getText(WebElement webElement) {
		return webElement.getText().trim();
	}

	/**
	 * Function to fetch method name of the function, from where this is invoked
	 * 
	 * @return ClassName
	 * @author Vikram
	 */
	protected String getClassName() {
		return new Throwable().getStackTrace()[1].getFileName().replace(".java", "");
	}

	/**
	 * Function to log details in the reporter
	 * 
	 * @param type
	 * @param message
	 */
	protected void logInfo(String type, String message) {
		try {
			if (type.equalsIgnoreCase("FAIL") || type.equalsIgnoreCase("PASS") || type.equalsIgnoreCase("FATAL")
					|| type.equalsIgnoreCase("ERROR")) {
				takeScreenshot(type.toUpperCase(), message);
				if (!type.equalsIgnoreCase("PASS")) {
					if (!rerun) {
						failedScenario.add(getTestData().get("Scenario"));
					}

					Assert.fail(message);
					getDriver().quit();

				}
			} else {
				Markup markup = MarkupHelper.createLabel(message, ExtentColor.WHITE);
				getReporter().log(Status.valueOf(type.toUpperCase()), markup);
			}
		} catch (NullPointerException n) {
			//System.out.println("System trying to log status before Extent reporter object is initialised ");
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());
		}
	}

	/**
	 * Function to take screenshot and log information in the reporter
	 * 
	 * @param type
	 * @param message
	 */
	protected void takeScreenshot(String type, String message) {
		try {
			Date d = new Date();
			String screenshotName = d.toString().replace(":", "_").replace(" ", "_");
			TakesScreenshot scrshot = ((TakesScreenshot) getDriver());
			File SrcFile = scrshot.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/reports/" + screenshotName + ".png";
			File destFile = new File(destination);
			FileUtils.copyFile(SrcFile, destFile);
			if (type.equalsIgnoreCase("FAIL"))
				getReporter().fail(message, MediaEntityBuilder.createScreenCaptureFromPath(destination).build());
			else if (type.equalsIgnoreCase("PASS"))
				getReporter().pass(message, MediaEntityBuilder.createScreenCaptureFromPath(destination).build());
			else if (type.equalsIgnoreCase("FATAL"))
				getReporter().fail(message, MediaEntityBuilder.createScreenCaptureFromPath(destination).build());
			else if (type.equalsIgnoreCase("ERROR"))
				getReporter().fail(message, MediaEntityBuilder.createScreenCaptureFromPath(destination).build());
		} catch (Exception e) {
			// logInfo("ERROR", "Error while executing method " + getMethodName() +" in the
			// class file "+getClassName()+ " with error: " + e.toString());
		}
	}

	/**
	 * Function to get current accessing webpage url
	 * 
	 * @return URL
	 */
	protected String getUrl() {
		return getDriver().getCurrentUrl();
	}

	/**
	 * Function to decode the encrypted password
	 * 
	 * @param codeWord
	 * @return password
	 */
	protected String decode(String codeWord) {
		return new PasswordProtect().decryptPass(codeWord, "Automation");
	}

	/**
	 * Function to load a url
	 * 
	 */
	protected void navigate(String url) {
		try {
			getDriver().get(url);
			waitTillPageLoads();
			logInfo("INFO", "Navigated to: " + suiteProp.getProperty("appUrl"));
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());
		}
	}

	protected void navigateback() {
		try {
			getDriver().navigate().back();
			logInfo("INFO", "navigated Back");
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());

		}
	}

	/**
	 * Function to clear text box
	 */
	protected void clear(WebElement element) {
		try {
			element.clear();
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());

		}
	}

	protected void clear(By element) {
		clear(getElement(element));

	}

	/**
	 * Function to open specified web browser
	 * 
	 * @updated Vikram
	 */
	protected void openBrowser(String nameOfTheExecution) {
		try {
			String browser = suiteProp.getProperty("browser");
			String session = suiteProp.getProperty("session");
			String chromeDriverPath = System.getProperty("user.dir") + "\\drivers\\chromedriver.exe";
			while (browser == null) {
				System.exit(0);
			}

			if (session.contains("EXISTING")) {
				System.setProperty("webdriver.chrome.driver", chromeDriverPath);
				ChromeOptions opt = new ChromeOptions();
				opt.setExperimentalOption("debuggerAddress", "localhost:63018 ");
				ChromeDriver driver = new ChromeDriver(opt);
				driver.navigate().to("");
				threadDriver.set(driver);
			} else {

				String defaultDownloadPath = null;
				if (suiteProp.get("ExecutionType").equals("Jenkins"))
					defaultDownloadPath = System.getProperty("user.dir") + "/"
							+ suiteProp.getProperty("default_DowanloadPath");
				else if (suiteProp.get("ExecutionType").equals("Local"))
					defaultDownloadPath = System.getProperty("user.dir") + "\\"
							+ suiteProp.getProperty("default_DowanloadPath").replace("/", "\\") + "\\";

				ChromeOptions options = new ChromeOptions();
				HashMap<String, Object> lChromePrefs = new HashMap<String, Object>();
				lChromePrefs.put("download.default_directory", defaultDownloadPath);
				lChromePrefs.put("browser.set_download_behavior",
						"{ behavior: 'allow' , downloadPath: '" + defaultDownloadPath + "'}");
				lChromePrefs.put("profile.default_content_setting_values.notifications", 1);
				options.setExperimentalOption("prefs", lChromePrefs);
				if (suiteProp.get("ExecutionType").equals("Local")) {
					System.setProperty("webdriver.chrome.driver", chromeDriverPath);
					ChromeDriver driver = new ChromeDriver(options);
					driver.manage().window().maximize();
					threadDriver.set(driver);

				} else if (suiteProp.get("ExecutionType").equals("Jenkins")) {
					DesiredCapabilities capabilities = new DesiredCapabilities();
					capabilities.setBrowserName("chrome");
					capabilities.setVersion("");
					capabilities.setCapability("enableVNC", true);
					capabilities.setCapability("enableVideo", false);
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					capabilities.setCapability("name", nameOfTheExecution);

					RemoteWebDriver driver = new RemoteWebDriver(URI.create("http://10.118.130.27:8080/wd/hub").toURL(),
							capabilities);
					driver.manage().window().maximize();
					threadDriver.set(driver);
				} else if (suiteProp.get("ExecutionType").equals("LOCAL-HEADLESS")) {
					options.addArguments("--disable-gpu");
					options.addArguments("--disable-extensions");
					options.addArguments("--disable-dev-shm-usage");
					options.addArguments("--headless");
					options.addArguments("--window-size=1920,1080");
					System.setProperty("webdriver.chrome.driver", chromeDriverPath);
					ChromeDriver driver = new ChromeDriver(options);
					driver.manage().window().maximize();
					threadDriver.set(driver);
				}
			}
			threadValidateData.set(new HashMap<>());
			wait("10000");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("DRIVER ISSUE");
		}
	}

	/**
	 * Function to get a last download file from a given path
	 * 
	 * @param dir
	 * @return
	 * @author Vikram
	 */
	protected File getLastDownloadedFile(String dir) {
		File fl = new File(dir);
		File[] files = fl.listFiles();
		long lastMod = Long.MIN_VALUE;
		File choice = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choice = file;
				lastMod = file.lastModified();
			}
		}
		return choice;
	}

	/**
	 * Function to delete All Files and Folder in a specified path
	 * 
	 * @param filePath
	 * @author Vikram
	 */
	public void deteleFileAndFolderInAFilePath(String filePath) {
		try {
			FileUtils.cleanDirectory(new File(filePath));
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());
		}
	}

	/**
	 * Function to name a file specified
	 * 
	 * @param sourceFilePath
	 * @param destinationFilePath
	 * @param nameOfTheFileWithExtension
	 * @author Vikram
	 */
	public void renamingAFileName(String sourceFilePath, String destinationFilePath,
			String nameOfTheFileWithExtension) {
		try {
			getLastDownloadedFile(sourceFilePath).renameTo(new File(destinationFilePath + nameOfTheFileWithExtension));
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());
		}
	}

	protected void waitTillPageLoads() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		String result = js.executeScript("return document.readyState").toString();
		for (int i = 0; i < 5; i++) {
			if (!result.equals("complete")) {
				sleepInSeconds(2);
			} else {
				break;
			}
		}

	}

	/**
	 * Function to get webelement of a By element
	 * 
	 * @param element
	 * @return
	 */
	protected WebElement getElement(By element) {
		WebElement webelement = null;
		try {
			webelement = new WebDriverWait(getDriver(), 5)
					.until(ExpectedConditions.visibilityOfElementLocated(element));
		} catch (Exception e) {
			logInfo("INFO", "WebElement Not found for: " + element + "||" + e.getMessage());
		}
		return webelement;
	}

	/**
	 * Function to switch to iframe with text to validate in given attibute
	 * 
	 * @param element
	 * @param text
	 * @param attribute
	 * @author Vikram
	 */

	protected void switchToFrame(By element, String text, String attribute) {
		try {
			for (WebElement webElement : getElements(element)) {
				if (webElement.getAttribute(attribute).contains(text)) {
					getDriver().switchTo().frame(webElement);
				}
			}
		} catch (Exception e) {
			logInfo("INFO", "WebElement Not found for: " + element + "||" + e.getMessage());
		}
	}

	/**
	 * Function to switch to iframe
	 * 
	 * @param element
	 * @param text
	 * @param attribute
	 * @author Kannan
	 */

	protected void switchToFrame(By element) {
		try {
			WebElement webElement = getElement(element);
			getDriver().switchTo().frame(webElement);
		} catch (Exception e) {
			logInfo("INFO", "WebElement Not found for: " + element + "||" + e.getMessage());
		}
	}

	/**
	 * Function to switch to parent/previous frame
	 * 
	 * @author Vikram
	 */
	protected void switchToParentFrame() {
		getDriver().switchTo().parentFrame();
	}

	/**
	 * Function to get list of webelement for a By element
	 * 
	 * @param element
	 * @return
	 */
	protected List<WebElement> getElements(By element) {
		List<WebElement> elementList = null;
		try {
			waitExplicitUntilVisible(5, element);
			elementList = getDriver().findElements(element);
		} catch (NoSuchElementException e) {
			logInfo("INFO", "WebElement Not found for: " + element + "||" + e.getMessage());
			e.printStackTrace();
		}
		return elementList;
	}

	/**
	 * Function to get elements without error logs
	 * 
	 * @param element
	 * @return
	 */
	protected List<WebElement> getElementsWithoutErrorLog(By element) {
		List<WebElement> elementList = null;
		try {
			waitExplicitUntilVisible(5, element);
			elementList = getDriver().findElements(element);
		} catch (NoSuchElementException e) {
		}
		return elementList;
	}

	/**
	 * Function to double an element
	 * 
	 * @param element
	 * @param suiteProp
	 * @updated Vikram
	 */
	protected void doubleClick(By element) {
		Actions actions = new Actions(getDriver());
		waitExplicitUntilVisible(5, element);
		scrollToAnElement(element);
		try {
			actions.doubleClick(getElement(element)).perform();
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("INFO", "Unable to double click on element " + element + "||" + e.getMessage());
		}
	}

	/**
	 * Function to input using Action
	 * 
	 * @param element
	 * @param value
	 * @updated Kannan
	 */
	protected void inputUsingAction(WebElement element, String value) {
		Actions actions = new Actions(getDriver());
		try {
			actions.moveToElement(element).sendKeys(value).build().perform();
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("INFO", "Unable to double click on element " + element + "||" + e.getMessage());
		}
	}

	protected void doubleClick(WebElement element) {
		Actions actions = new Actions(getDriver());
		new WebDriverWait(getDriver(), 10).until(ExpectedConditions.visibilityOf(element));
		scrollToAnElement(element);
		try {
			actions.doubleClick(element).perform();
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("INFO", "Unable to double click on element " + element + "||" + e.getMessage());
		}
	}

	/**
	 * Function to click an element
	 * 
	 * @param element
	 * @author Vikram
	 */
	protected void click(By element) {
		click(getElement(element));
		logInfo("Info", "Clicked on " + element);
	}

	/**
	 * Function to click a WebElement
	 * 
	 * @param element
	 * @author Vikram
	 */
	protected void click(WebElement element) {
		try {
			element.click();
		} catch (Exception e) {
			try {
				((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", element);
			} catch (Exception e1) {
				try {
					new Actions(getDriver()).moveToElement(element).click(element).perform();
				} catch (Exception e2) {
					e2.printStackTrace();
					logInfo("INFO", "Error while clicking on Button:" + element + "||" + e.getMessage());
				}
			}
		}
	}

	/**
	 * Function to scroll to a particular element
	 * 
	 * @param element
	 * @author Vikram
	 */
	protected void scrollToAnElement(By element) {
		scrollToAnElement(getElement(element));
	}

	/**
	 * Function to scroll to a particular WebElement
	 * 
	 * @param element
	 * @author Vikram
	 */
	protected void scrollToAnElement(WebElement element) {
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	/**
	 * Function to pass a value to a WebElement
	 * 
	 * @param element
	 * @param testData
	 * @updated Vikram
	 */
	protected void input(By element, String testData) {
		waitExplicitUntilVisible(10, element);
		input(getElement(element), testData);
	}

	/**
	 * Function to pass a value to a WebElement
	 * 
	 * @param element
	 * @param testData
	 * @updated Vikram
	 */
	protected void input(WebElement element, String testData) {
		try {
			element.sendKeys(testData);
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("INFO", "Unable to send Keys to :" + testData + "||" + e.getMessage());
		}
	}

	/**
	 * Function to perform keyboard actions
	 * 
	 * @param actionToPerform
	 * @updated Vikram
	 */
	protected void keyboardActions(By element, Keys actionToPerform) {
		keyboardActions(getElement(element), actionToPerform);
	}

	/**
	 * Function to perform keyboard actions
	 * 
	 * @param actionToPerform
	 * @updated Vikram
	 */
	protected void keyboardActions(WebElement element, Keys actionToPerform) {
		new Actions(getDriver()).moveToElement(element).sendKeys(actionToPerform).build().perform();
	}

	/**
	 * Function to check checkbox for specified element
	 * 
	 * @param element
	 * @updated Vikram
	 */
	protected void selectCheckbox(By element) {
		waitExplicitUntilVisible(5, element);
		WebElement webelement = getElement(element);
		if (webelement.isSelected()) {
			logInfo("INFO", "check box already selected");
		} else {
			try {
				webelement.click();
			} catch (Exception e) {
				e.printStackTrace();
				logInfo("INFO", "Unable to Select Checkbox" + element + "||" + e.getMessage());
			}
		}
	}

	/**
	 * Function to select a value from a dropdown for specified element
	 * 
	 * @param element
	 * @param type
	 * @param text
	 * @updated Vikram
	 */
	protected void selectDropdown(WebElement element, String type, String text) {
		Select select = new Select(element);
		if (type.equalsIgnoreCase("Value")) {
			select.selectByValue(text);
		} else if (type.equalsIgnoreCase("Index")) {
			int num = Integer.valueOf(text);
			select.selectByIndex(num);
		} else if (type.equalsIgnoreCase("VisibleText")) {
			select.selectByVisibleText(text);
		} else {
			logInfo("ERROR", "No such value:" + text);
		}
	}

	/**
	 * Function to select a value from a dropdown for specified element
	 * 
	 * @param element
	 * @param type
	 * @param text
	 * @updated Vikram
	 */
	protected void selectDropdown(By element, String type, String text) {
		selectDropdown(getElement(element), type, text);
	}

	/**
	 * Function to click based on the provided text from multiple similar elements
	 * 
	 * @param element
	 * @param OptionToChoose
	 * @return
	 * @author Vikram
	 */
	protected boolean clickElementFromMultipleSimilarElements(By element, String OptionToChoose) {
		boolean flag = false;
		try {
			int counter = 0;
			do {
				counter++;
				for (WebElement eachElement : getElementsWithoutErrorLog(element)) {
					if (getText(eachElement).trim().equals(OptionToChoose)) {
						flag = true;
						System.out.println("Clicking on element with text: "+getText(eachElement));
						click(eachElement);
						break;
					}
				}
			} while (!(flag || counter < 2));
			if (flag)
				logInfo("INFO", "Clicked on '" + OptionToChoose + "'");
			else
				logInfo("INFO", "No such value:" + OptionToChoose);
		} catch (StaleElementReferenceException e) {
			logInfo("INFO", "Got into catch block due to 'StaleElementException'");
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());
		}
		return flag;
	}

	/**
	 * Function to validate presence of text from multiple elements
	 * 
	 * @param validationElement
	 * @param validationText
	 * @return
	 * @author Vikram
	 */
	protected boolean validatePresenceOfTextFromMultipleElements(By validationElement, String validationText) {
		boolean result = false;
		try {
			waitExplicitUntilPresence(10, validationElement);
			for (WebElement accountNameWebelement : getElements(validationElement)) {
				if (result)
					break;
				if (getText(accountNameWebelement).equals(validationText))
					result = true;
			}
		} catch (StaleElementReferenceException s) {
			validatePresenceOfTextFromMultipleElements(validationElement, validationText);
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());
		}
		return result;
	}

	/**
	 * Function to verify presence of a webelement
	 * 
	 * @param element
	 * @return true/false
	 */
	protected boolean verifyElementPresent(By element) {

		try {
			if (getElement(element).isDisplayed() || getElement(element) != null) {
				// scrollToAnElement(element);
				return true;
			} else {
				logInfo("INFO", "Unable to verify the presence of Webelement " + element);
				return false;
			}
		} catch (Exception e) {
			logInfo("INFO", "Unable to verify the presence of Webelement " + element);
			return false;
		}
	}

	/**
	 * Function to verify presence of a webelement
	 * 
	 * @param element
	 * @return true/false
	 */
	protected boolean verifyElementPresentWithWait(int seconds, By element) {

		try {
			waitExplicitUntilVisibleWithException(seconds, element);
			if (getElement(element).isDisplayed() || getElement(element) != null) {
				scrollToAnElement(element);
				return true;
			} else {
				logInfo("INFO", "Unable to verify the presence of Webelement " + element);
				return false;
			}
		} catch (Exception e) {
			logInfo("INFO", "Unable to verify the presence of Webelement " + element);
			return false;
		}
	}

	/**
	 * Function to verify presence of a webelement
	 * 
	 * @param element
	 * @return true/false
	 * @author Vikram
	 */
	protected boolean verifyElementPresentWithoutException(By element) {
		try {
			if (getElement(element).isDisplayed() || getElement(element) != null) {
				return true;
			} else {
				logInfo("ERROR", "Unable to verify the presence of Webelement " + element);
				return false;
			}
		} catch (Exception e) {
			logInfo("INFO", "Unable to verify the presence of Webelement " + element);
			return false;
		}
	}

	/**
	 * Function to absence presence of a webelement
	 * 
	 * @param element
	 * @return true/false
	 * @author Vikram
	 */
	protected boolean verifyElementNotPresentWithouException(By element) {
		try {
			if (!getElement(element).isDisplayed() || getElement(element) != null) {
				return true;
			} else {
				logInfo("ERROR", "unable to verify the absence of Webelement " + element);
				return false;
			}
		} catch (Exception e) {
			logInfo("ERROR", "Unable to verify the absence of Webelement " + element);
			return true;
		}
	}

	/**
	 * Function to absence presence of a webelement
	 * 
	 * @param element
	 * @return true/false
	 */
	protected boolean verifyElementNotPresent(By element) {
		if (!getElement(element).isDisplayed() || getElement(element) != null) {
			return true;
		} else {
			logInfo("ERROR", "unable to verify the absence of Webelement " + element);
			return false;
		}
	}

	/**
	 * Function to verify presence of exact text of a webelement
	 * 
	 * @param element
	 * @param textToVerify
	 * @return true/false
	 * @author Vikram
	 */
	protected boolean verifyExactTextPresent(By webElement, String textToVerify) {
		String actualText = getText(webElement);
		if (actualText.trim().equalsIgnoreCase(textToVerify)) {
			return true;
		} else {
			logInfo("ERROR", "Text Not Verified in Page:" + textToVerify + "Actual Text is: " + actualText.trim());
			return false;
		}
	}

	/**
	 * Function to verify presence of partial text of a webelement
	 * 
	 * @param element
	 * @param textToVerify
	 * @return true/false
	 * @author Vikram
	 */
	protected boolean verifyPartialTextPresent(By webElement, String textToVerify) {
		String actualText = getText(webElement);
		if (actualText.contains(textToVerify)) {
			return true;
		} else {
			logInfo("ERROR", "Text Not Verified in Page:" + textToVerify);
			return false;
		}
	}

	/**
	 * Function to verify presence of specific text of a webelement without error
	 * log
	 * 
	 * @param element
	 * @param textToVerify
	 * @return true/false
	 * @author Vikram
	 */
	protected boolean verifyTextPresentwithouterrorlog(By element, String textToVerify) {
		String actualText = getText(element);
		if (actualText.equalsIgnoreCase(textToVerify)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Function to refesh the webpage
	 * 
	 * @author Vikram
	 */
	protected void refreshPage() {
		navigate(getUrl());
	}

	/**
	 * Function to verify presence of webelement
	 * 
	 * @param element
	 * @return true/false
	 * @updated Vikram
	 */
	protected boolean isElementPresent(By element) {
		boolean flag = false;
		try {
			waitExplicitUntilVisible(10, element);
			if (getElement(element).isDisplayed()) {
				flag = true;
			}
		} catch (Exception e) {
			logInfo("ERROR", "No such Element:" + element);
		}
		return flag;
	}

	/**
	 * Function to verify presence of webelement
	 * 
	 * @param element
	 * @return true/false
	 * @author Vikram
	 */
	protected boolean isElementPresentWithouErrorLog(By element) {
		boolean flag = false;
		try {
			if (getDriver().findElement(element).isDisplayed()) {
				flag = true;
			}
		} catch (Exception e) {
		}
		return flag;
	}

	/**
	 * Function to verify string is empty/blank
	 * 
	 * @param string
	 * @return true/false
	 */
	protected boolean isBlank(String string) {
		return StringUtils.isBlank(string);
	}

	/**
	 * Function to use Explicit wait for specified time
	 * 
	 * @param milliseconds
	 */
	protected void wait(String milliseconds) {
		Long num = Long.valueOf(milliseconds);
		getDriver().manage().timeouts().implicitlyWait(num, TimeUnit.MILLISECONDS);
	}

	/**
	 * Function to use sleep for mentioned time
	 * 
	 * @param milliseconds
	 * @throws InterruptedException
	 * @author Vikram
	 */
	protected void sleepInSeconds(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Function to wait until webelement is visible for specified time
	 * 
	 * @param seconds
	 * @param webElement
	 * @updated Vikram
	 */
	protected void waitExplicitUntilVisible(int seconds, By webElement) {
		try {
			waitExplicitUntilVisibleWithException(seconds, webElement);
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
					+ "with error:" + e.toString());
		}
	}

	/**
	 * Function to wait until webelement is visible for specified time and throws
	 * exception if any
	 * 
	 * @param seconds
	 * @param webElement
	 * @author Vikram
	 */
	protected void waitExplicitUntilVisibleWithException(int seconds, By webElement) {
		new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.visibilityOfElementLocated(webElement));
	}

	/**
	 * Function to wait until iFrame to be visible
	 * 
	 * @param seconds
	 * @param frameNumber
	 * @author Vikram
	 */
	protected void waitExplicitUntilFrameVisibleWithException(int seconds, By element) {
		new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
	}

	/**
	 * Function to wait until iFrame to be visible
	 * 
	 * @param seconds
	 * @param frameNumber
	 * @author Vikram
	 */
	protected void waitExplicitUntilFrameVisibleWithException(int seconds, int frameNumber) {
		new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameNumber));
	}

	/**
	 * Function to wait until webelement is presence in DOM for specified time
	 * 
	 * @param seconds
	 * @param webElement
	 * @author Vikram
	 */
	protected void waitExplicitUntilPresence(int seconds, By webElement) {
		try {
			new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.presenceOfElementLocated(webElement));
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method\n" + getMethodName() + "\nin the class file \n"
					+ getClassName() + "\nwith error:" + e.toString());
		}
	}

	/**
	 * Function to wait until webelement is clickable for specified time
	 * 
	 * @param seconds
	 * @param webElement
	 * @updated Vikram
	 */
	protected void waitExplicitUntilClickable(int seconds, By webElement) {
		try {
			waitExplicitUntilClickableWithException(seconds, webElement);
			logInfo("INFO", "Waited for element " + webElement + " for " + seconds + " seconds to be clickable");
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
					+ "with error:" + e.toString());
		}
	}

	/**
	 * Function to wait until webelement is clickable for specified time
	 * 
	 * @param seconds
	 * @param webElement
	 * @author Vikram
	 */
	protected void waitExplicitUntilClickableWithException(int seconds, By webElement) {
		WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(webElement));
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
	}

	/**
	 * Function to wait until webelement is invisible for specified time
	 * 
	 * @param seconds
	 * @param webElement
	 * @updated Vikram
	 */
	protected void waitExplicitUntilInvisible(int seconds, By webElement) {
		try {
			new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.invisibilityOfElementLocated(webElement));
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
					+ "with error:" + e.toString());
		}
	}

	/**
	 * Function to get random string restricted to 5 character length
	 * 
	 * @return random string
	 * @author Vikram
	 */
	@SuppressWarnings("static-access")
	protected String getRandomString() {
		return new RandomStringUtils().randomAlphabetic(5);
	}

	/**
	 * Function to get random string for specified character length
	 * 
	 * @param charCount
	 * @return random string
	 * @author Vikram
	 */
	@SuppressWarnings("static-access")
	protected String getRandomString(int charCount) {
		return new RandomStringUtils().random(charCount);
	}

	/**
	 * Function to add specific months to specified date
	 * 
	 * @param date
	 * @param month
	 * @return Date in EST timezone
	 * @author Vikram
	 */
	protected Date addMonthsToDate(Date date, int month) {
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(date);
			cal.add(Calendar.MONTH, month);
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
					+ "with error:" + e.toString());
		}
		return cal.getTime();
	}

	/**
	 * Function to get tomorrow's date
	 * 
	 * @return Date in 'MMM d, yyyy' format and in EST timezone
	 * @author Vikram
	 */
	protected String gettomorrowDate() {
		return DateTimeFormatter.ofPattern("MMM d, yyyy")
				.format(LocalDateTime.now(TimeZone.getTimeZone("EST").toZoneId()).plusDays(1)).toString();
	}

	/**
	 * Function to reduce specified hours from current time
	 * 
	 * @param hours
	 * @return Date in 'M/dd/yyyy, h:mm a' format and in EST timezone
	 * @author Vikram
	 */
	protected String getPastDateTime(double hours) {
		SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy, h:mm a");
		sdf.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		return sdf.format(new Date((long) (System.currentTimeMillis() - (hours * (60 * 60 * 1000))))).toString();
	}

	/**
	 * Function to Increment/Decrement specified
	 * Month(s)/Week(s)/Hour(s)/Minute(s)/Second(s) from current Date&time
	 * 
	 * @param timeFormat
	 * @param addEntity
	 * @param amountToAdd
	 * @return String in specified format and with ET timezone
	 * @author Vikram
	 */
	protected static String getDateAndTime(String timeFormat, String entityType, int amountToAdd) {
		Calendar calObj = Calendar.getInstance();
		SimpleDateFormat simpleDateFormatObject = new SimpleDateFormat(timeFormat);
		simpleDateFormatObject.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		switch (entityType.toLowerCase()) {
		case "years":
			calObj.set(Calendar.YEAR, calObj.get(Calendar.YEAR) + amountToAdd);
			break;
		case "months":
			calObj.set(Calendar.MONTH, calObj.get(Calendar.MONTH) + amountToAdd);
			break;
		case "days":
			calObj.set(Calendar.DATE, calObj.get(Calendar.DATE) + amountToAdd);
			break;
		case "date":
			calObj.set(Calendar.DATE, calObj.get(Calendar.DATE) - amountToAdd);
			break;
		case "hours":
			calObj.set(Calendar.HOUR_OF_DAY, calObj.get(Calendar.HOUR_OF_DAY) + amountToAdd);
			break;
		case "minutes":
			calObj.set(Calendar.MINUTE, calObj.get(Calendar.MINUTE) + amountToAdd);
			break;
		case "seconds":
			calObj.set(Calendar.SECOND, calObj.get(Calendar.SECOND) + amountToAdd);
			break;
		default:
			break;
		}
		return simpleDateFormatObject.format(calObj.getTime());
	}

	/**
	 * 
	 * This method is used to wait for new windows
	 * 
	 * @param timeout
	 * @return boolean
	 * @author SARANRA
	 */
	public boolean waitForNewWindow(int timeout) {
		boolean flag = false;
		try {

			WebDriverWait wait = new WebDriverWait(getDriver(), timeout);
			Function<WebDriver, Boolean> function = new Function<WebDriver, Boolean>() {

				@Override
				public Boolean apply(WebDriver driver) {
					Set<String> winId = getDriver().getWindowHandles();
					if (winId.size() > 1) {
						return true;
					} else
						return false;
				}

			};
			try {
				wait.until(function);
				flag = true;
				return flag;
			} catch (Exception e) {
				logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
						+ "with error:" + e.toString());
				return flag;
			}
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
					+ "with error:" + e.toString());
			;
			return false;
		}
	}

	/**
	 * This is used to generate random number of desired length
	 * 
	 * @param len
	 * @return
	 */
	public String getRandomNumber(int len) {
		try {
			Random random = new Random();
			int randomNumber = 0;
			boolean loop = true;
			while (loop) {
				randomNumber = random.nextInt();
				if (Integer.toString(randomNumber).length() == len && !Integer.toString(randomNumber).startsWith("-")) {
					loop = false;
				}
			}
			return Integer.toString(randomNumber);
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
					+ "with error:" + e.toString());
			;
			return null;
		}
	}

	/**
	 * This method is used to check if java alert is present or not
	 * 
	 * @param
	 * @return Boolean
	 * @author SARANRA
	 * 
	 */
	public boolean isAlertPresent() {
		try {
			getDriver().switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	/**
	 * This method is used to get the future date in desired format
	 * 
	 * @param dateFormat
	 * @param NoOfMonths
	 * @return date
	 * @author KANNAN
	 * 
	 */
	protected String getFutureDate(String dateFormat, int NoOfMonths) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, NoOfMonths);
		Date date = calendar.getTime();
		SimpleDateFormat format1 = new SimpleDateFormat(dateFormat);
		String FutureDate = format1.format(date);
		return FutureDate;
	}

	/**
	 * This method is used to get the current date
	 * 
	 * @param dateFormat
	 * @return date
	 * @author KANNAN
	 * 
	 */
	protected String getCurrentDate(String dateFormat) {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		SimpleDateFormat format1 = new SimpleDateFormat(dateFormat);
		String FutureDate = format1.format(date);
		return FutureDate;
	}

	/**
	 * Function to scroll down a page
	 * 
	 * @author KANNAN
	 */

	protected void pageScrollDown() {
		JavascriptExecutor jse = (JavascriptExecutor) getDriver();
		jse.executeScript("window.scrollBy(0,250)");
	}

	/**
	 * Function to open URL in new TAB
	 * 
	 * @author KANNAN
	 */

	protected void openURLInNewTab(String url) {
		((JavascriptExecutor) getDriver()).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs.get(2));
		getDriver().get(url);
	}

	/**
	 * Function to open URL in new TAB
	 * 
	 * @author KANNAN
	 */

	protected void openURLInNewTab(String url, int tabNumber) {
		((JavascriptExecutor) getDriver()).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs.get(tabNumber));
		getDriver().get(url);
	}

	/**
	 * Function to switch to TAB based on index
	 * 
	 * @author KANNAN
	 */

	protected void switchToTab(int tabIndex) {
		ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs.get(tabIndex));
	}

	/**
	 * Function to get number of tabs opened
	 * 
	 * @author KANNAN
	 */

	protected int getTabCount() {
		ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
		System.out.println("Number of Tabs:" + tabs.size());
		return tabs.size();
	}

	/**
	 * Function to get all fileNames in a Folder
	 * 
	 * @author KANNAN
	 */
	protected String getAllFilesList() {
		File folder = new File(System.getProperty("user.dir") + "\\src\\test\\resources\\chatterAttachments");
		File[] listOfFiles = folder.listFiles();
		StringBuffer allFilesPath = new StringBuffer();
		StringBuffer allFilesNames = new StringBuffer();

		String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\chatterAttachments";
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				allFilesPath = allFilesPath.append("\"" + filePath + "\\" + "" + listOfFiles[i].getName() + "\" ");
				allFilesNames = allFilesNames.append("\"" + listOfFiles[i].getName() + "\" ");
			}

		}
		return allFilesNames.toString();
	}

	protected List<String> getAllFilesListWithExtension() {
		File folder = new File(System.getProperty("user.dir") + "\\src\\test\\resources\\chatterAttachments");
		File[] listOfFiles = folder.listFiles();
		List<String> fileNames = new ArrayList<String>();
		String fileNameTemp = null;
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				fileNameTemp = listOfFiles[i].getName();
				fileNames.add(fileNameTemp);
			}

		}
		return fileNames;
	}

	protected void uploadFile(String files) {
		try {
			Robot robot = new Robot();

			StringSelection str = new StringSelection(files);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);

			// press Contol+V for pasting
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.delay(500);
			robot.keyPress(KeyEvent.VK_V);

			// release Contol+V for pasting
			robot.delay(1500);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.delay(500);
			robot.keyRelease(KeyEvent.VK_V);

			// for pressing and releasing Enter
			robot.delay(1500);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.delay(500);
			robot.keyRelease(KeyEvent.VK_ENTER);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void closeWindow() {
		getDriver().close();
		for (String winHandle : getDriver().getWindowHandles()) {
			getDriver().switchTo().window(winHandle);
		}
	}

	/**
	 * Function to validate a method based on boolean value
	 * 
	 * @param validationBoolean
	 * @param message
	 */

	public void validateFuntion(boolean flag, String message) {
		if (flag) {
			logInfo("PASS", "PASSED: " + message);
		} else {
			System.err.println("FAILED --> " + message);
			logInfo("FAIL", "FAILED --> " + message);
		}
	}

	/**
	 * Function to wait until webelement is clickable and click for specified time
	 * 
	 * @param seconds
	 * @param By
	 */
	protected void wait_And_Click(int seconds, By by) {
		waitExplicitUntilClickable(seconds, by);
		click(by);
	}

	/**
	 * Function to wait until webelement is present and click for specified time
	 * 
	 * @param seconds
	 * @param By
	 */
	protected void WaitTillPresent_And_Click(int seconds, By by) {
		waitExplicitUntilPresence(seconds, by);
		click(by);
	}

	/**
	 * Function to wait till alert is displayed
	 * 
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unused")
	protected void waitForAlert(int seconds) throws InterruptedException {
		int i = 0;
		while (i++ < seconds) {
			try {
				Alert alert = getDriver().switchTo().alert();
				break;
			} catch (NoAlertPresentException e) {
				Thread.sleep(1000);
				continue;
			}
		}
	}

	protected void clearbyJavascript(By element) {
		clearbyJavascript(getElement(element));

	}

	/**
	 * Function to clear text box
	 * 
	 * @param element
	 * @author Ajay
	 */
	protected void clearbyJavascript(WebElement element) {
		try {
			element.click();
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].value='';", element);
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("INFO", "Error while clearing the text box :" + element + "||" + e.getMessage());

		}
	}

	/**
	 * Function to verify text are equal
	 * 
	 * @param element
	 * @author Kannan
	 */
	protected boolean verifyTextEquals(By element, String valueToCompare) {
		boolean flag = false;
		String valueFromApplication = null;
		try {
			valueFromApplication = getText(element);
			flag = valueFromApplication.equals(valueToCompare);
			System.out.println("result: " + flag + " || Actual: " + valueFromApplication + " " + " || Expected: "
					+ valueToCompare);
		} catch (Exception e) {
			flag = false;
			System.out.println("result: " + flag + " || Actual: " + valueFromApplication + " " + " || Expected: "
					+ valueToCompare);
		}
		return flag;
	}

	// Methods From VzC Data Driven

	/**
	 * Function to perform Move to specific webelement
	 * 
	 * @param element
	 * @updated Vikram
	 */
	protected void moveToElement(By element) {
		new Actions(getDriver()).moveToElement(getElement(element)).perform();
	}

	/**
	 * Function to provide parent window name
	 * 
	 * @return
	 */
	protected String getParentWindow() {
		return getDriver().getWindowHandle();
	}

	/**
	 * Function to give all windows opened
	 * 
	 * @return
	 */
	protected Set<String> getAllWindows() {
		return getDriver().getWindowHandles();
	}

	/**
	 * Function to switch to given window
	 */
	protected void switchToWindow(String windowName) {
		getDriver().switchTo().window(windowName);
	}

	/**
	 * Close currentWindow and Switch to desired Window
	 */
	protected void closeCurrentWindowAndSwitchToWindow(String windowName) {
		getDriver().close();
		getDriver().switchTo().window(windowName);
	}

	/**
	 * Function to wait until text for specified webelement is presence in DOM for
	 * specified time
	 * 
	 * @param seconds
	 * @param webElement
	 * @author Vikram
	 */
	protected void waitExplicitUntilPresenceOfTextInWebelement(int seconds, By webElement, String validationText) {
		try {
			waitExplicitUntilVisible(seconds, webElement);
			new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.textToBe(webElement, validationText));
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
					+ "with error:" + e.toString());
		}
	}

	/**
	 * Function to wait until webelement is presence in DOM for specified time
	 * 
	 * @param seconds
	 * @param webElement
	 * @author Vikram
	 */
	protected void waitExplicitUntilPresenceOfWebElement(int seconds, By webElement) {
		try {
			new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.presenceOfElementLocated(webElement));
		} catch (Exception e) {
			logInfo("INFO", "Error while executing method" + getMethodName() + "in the class file " + getClassName()
					+ "with error:" + e.toString());
		}
	}

	protected String getCurrentDateTime() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now).replace("/", "").replace(":", "").replace(" ", "");
		return date;
	}

	protected String getAttribute(By webElement, String attribute) {
		return getElement(webElement).getAttribute(attribute);
	}

	public void closeBrowser() {
		if (getDriver() != null)
			getDriver().quit();
	}

	/**
	 * Function to generate Random Names, Address, etc
	 * 
	 * @param value
	 * @author Kannan
	 */

	public String generateTestData(String value) {
		String temp = null;
		try {

			Faker faker = new Faker(new Locale("en-US"));
			if (value.equalsIgnoreCase("firstname")) {
				temp = faker.name().firstName();
			} else if (value.equalsIgnoreCase("lastname")) {
				temp = faker.name().firstName();
			} else if (value.equalsIgnoreCase("fullname")) {
				temp = faker.name().fullName();
			} else if (value.equalsIgnoreCase("company")) {
				temp = faker.company().name();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;

	}

	/**
	 * Function to expand shadow room element
	 * 
	 * @param value
	 * @author Kannan
	 * @throws InterruptedException
	 */
	public WebElement expandShadowHostElement(String csslocator) throws InterruptedException {
		WebElement shadowDomHostElement = getDriver().findElement(By.cssSelector(csslocator));
		WebElement shadowRoomElement = (WebElement) ((JavascriptExecutor) getDriver())
				.executeScript("return arguments[0].shadowRoot", shadowDomHostElement);
		Thread.sleep(500);
		return shadowRoomElement;
	}

	/**
	 * Function to expand shadow room element
	 * 
	 * @param value
	 * @author Kannan
	 * @throws InterruptedException
	 */
	public WebElement expandShadowRootElement(WebElement previousRootElement, String csslocator)
			throws InterruptedException {
		WebElement shadowDomHostElement1 = previousRootElement.findElement(By.cssSelector(csslocator));
		WebElement last1 = (WebElement) ((JavascriptExecutor) getDriver())
				.executeScript("return arguments[0].shadowRoot", shadowDomHostElement1);
		Thread.sleep(500);
		return last1;
	}

	/**
	 * Function to expand shadow room element
	 * 
	 * @param shadowparent
	 * @author Ramya
	 */
	protected WebElement getElement(WebElement element) {
		try {
			element = new WebDriverWait(getDriver(), 10).until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			logInfo("INFO", "WebElement Not found for: " + element + "||" + e.getMessage());
		}
		return element;
	}

	public WebElement expandRootElement(WebElement shadowparent) {
		WebElement ele = (WebElement) ((JavascriptExecutor) threadDriver.get())
				.executeScript("return arguments[0].shadowRoot", shadowparent);
		return ele;
	}

	/**
	 * Function to update counter property
	 * 
	 * @param value
	 * @author Kannan
	 */
	public String getCounterValue() {
		Properties properties = new Properties();
		String counterValue = "0";
		String value = null;
		try {
			String propertiesFilePath = ("src/test/resources/counter.properties");
			FileInputStream fis = new FileInputStream(propertiesFilePath);
			properties.load(fis);
			counterValue = properties.getProperty("counter");

			int temp = Integer.parseInt(counterValue) + 1;
			value = String.valueOf(temp);

			if (Integer.parseInt(counterValue) <= 9) {
				value = "0" + value;
			}

			properties.setProperty("counter", value);

			FileOutputStream fos = new FileOutputStream(propertiesFilePath);
			properties.store(fos, null);
			System.out.println("counter updated to -> " + value);
		} catch (Exception e) {
			System.out.println("FAILED");
		}
		return counterValue;
	}

	public void robotnumbers(int quantity) throws AWTException {
		Robot robot = new Robot();
		String dem = Integer.toString(quantity);
		char charecrdem[] = dem.toCharArray();
		for (char ah : charecrdem) {
			if (ah == '0') {
				robot.keyPress(KeyEvent.VK_0);
				robot.keyRelease(KeyEvent.VK_0);
			}

			if (ah == '1') {
				robot.keyPress(KeyEvent.VK_1);
				robot.keyRelease(KeyEvent.VK_1);
			}

			if (ah == '2') {
				robot.keyPress(KeyEvent.VK_2);
				robot.keyRelease(KeyEvent.VK_2);
			}
			if (ah == '3') {
				robot.keyPress(KeyEvent.VK_3);
				robot.keyRelease(KeyEvent.VK_3);
			}
			if (ah == '4') {
				robot.keyPress(KeyEvent.VK_4);
				robot.keyRelease(KeyEvent.VK_4);
			}
			if (ah == '5') {
				robot.keyPress(KeyEvent.VK_5);
				robot.keyRelease(KeyEvent.VK_5);
			}
			if (ah == '6') {
				robot.keyPress(KeyEvent.VK_6);
				robot.keyRelease(KeyEvent.VK_6);
			}
			if (ah == '7') {
				robot.keyPress(KeyEvent.VK_7);
				robot.keyRelease(KeyEvent.VK_7);
			}
			if (ah == '8') {
				robot.keyPress(KeyEvent.VK_8);
				robot.keyRelease(KeyEvent.VK_8);
			}
			if (ah == '9') {
				robot.keyPress(KeyEvent.VK_9);
				robot.keyRelease(KeyEvent.VK_9);
			}

		}

	}

}
