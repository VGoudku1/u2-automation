package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.BrowsePhonesPageObjects;

public class BrowsePhonesPage extends GenericKeywords{
	
	BrowsePhonesPageObjects objBrowsePhonesPageObj = new BrowsePhonesPageObjects();
	
	public void clickPhonesByName(String phoneName){
		clickElementFromMultipleSimilarElements(objBrowsePhonesPageObj.BrowsePhonesByName, phoneName);
	}
}
