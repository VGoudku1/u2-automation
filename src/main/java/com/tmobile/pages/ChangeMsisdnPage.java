package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.ChangeMsisdnPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class ChangeMsisdnPage extends GenericKeywords {

	ChangeMsisdnPageObjects objChangeMsisdnPageObj = new ChangeMsisdnPageObjects();
	CommonUtilFunctions utilFunctions = new CommonUtilFunctions();

	public void clickOnGetNewNumberButton() {
		waitExplicitUntilClickable(10, objChangeMsisdnPageObj.GetANewNumberButton);
		click(objChangeMsisdnPageObj.GetANewNumberButton);
	}

	public void enterZipOrAreaCode(String zipCode) {
		input(objChangeMsisdnPageObj.EnterZiporAreaCode,zipCode);
		click(objChangeMsisdnPageObj.SearchButton);
		utilFunctions.waitForSpinnerToDisappear(10);
	}

	public void clickGetThisButton(String buttonText) {
		clickElementFromMultipleSimilarElements(objChangeMsisdnPageObj.GetThisNumberButton, buttonText);
	}

	public void clickSubmitRequest(String buttonText) {
		clickElementFromMultipleSimilarElements(objChangeMsisdnPageObj.SubmitRequestButton, buttonText);
	}

}
