package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.AccountSetUpPageObjects;

public class AccountSetUpPage extends GenericKeywords {
	AccountSetUpPageObjects objAccountSetupObj = new AccountSetUpPageObjects();
	
	public void clickContinueWithAccountButton() {
		waitExplicitUntilClickable(4, objAccountSetupObj.ClickContinueWithAccountButton);
		click(objAccountSetupObj.ClickContinueWithAccountButton);

	}
}
