package com.tmobile.pages;

import java.io.IOException;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.excel.ExcelReader;
import com.tmobile.pagesObjectRepository.OrderConfirmationPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class OrderConfirmationPage extends GenericKeywords{
	OrderConfirmationPageObjects objOrderConfirmationPageObj = new OrderConfirmationPageObjects();
	CommonUtilFunctions utilFunctions = new CommonUtilFunctions();
	
	ExcelReader excelReader = new ExcelReader(suiteProp.getProperty("dataSheet"));
	
	public void verifyOrderSubmitted() {
		verifyExactTextPresent(objOrderConfirmationPageObj.OrderConfrimPageTitle, "Order Submitted!");
	}
	
	public void verifyOrderSuccess(String sucessMessage) {
		verifyExactTextPresent(objOrderConfirmationPageObj.SuspendSuccessTitle, sucessMessage);
	}
	
	public void suspendOrderNumberGenerated() throws IOException {
		String orderNum = getText(objOrderConfirmationPageObj.SuspendOrderNumber);
		String[] orderNumInt = orderNum.split("#");
		System.out.println(orderNumInt[1]);
		utilFunctions.writeOrderNumberToExcel(orderNumInt[1]);
		
	}
	
	public void cancelOrderNumberGenerated() throws IOException {
		String orderNum = getText(objOrderConfirmationPageObj.CancelOrderNumber);
		String[] orderNumInt = orderNum.split("#");
		System.out.println(orderNumInt[1]);
		utilFunctions.writeOrderNumberToExcel(orderNumInt[1]);
		
	}
	
	public void orderNumberGenerated() throws IOException {
		String orderNum = getText(objOrderConfirmationPageObj.OrderNumber);
		String[] orderNumInt = orderNum.split("#");
		System.out.println(orderNumInt[1]);
		utilFunctions.writeOrderNumberToExcel(orderNumInt[1]);
		
	}
	
	public void changeSimOrderNumberGenerated() throws IOException {
		String orderNum = getText(objOrderConfirmationPageObj.ChangeSimOrderNumber);
		String[] orderNumInt = orderNum.split("#");
		System.out.println(orderNumInt[1]);
		utilFunctions.writeOrderNumberToExcel(orderNumInt[1]);
		
	}
	
	public void blockIMEIOrderNumberGenerated() throws IOException {
		String orderNum = getText(objOrderConfirmationPageObj.BlockIMEIOrderNumber);
		String[] orderNumInt = orderNum.split("#");
		System.out.println(orderNumInt[1]);
		utilFunctions.writeOrderNumberToExcel(orderNumInt[1]);
		
	}

}
