package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.ResumeCancellationPageObjects;

public class ResumeCancellationPage extends GenericKeywords {
	
	ResumeCancellationPageObjects objResumeCancellationPageObj = new ResumeCancellationPageObjects();
	public void clickNextButton() {
		click(objResumeCancellationPageObj.NextButton);
	}

}
