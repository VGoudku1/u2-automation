package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.OrderSummaryPageObjects;

public class OrderSummaryPage extends GenericKeywords {
	
	OrderSummaryPageObjects objOrderSummaryPageObj = new OrderSummaryPageObjects();
	
	public void verifyOrderSummary() {
		verifyExactTextPresent(objOrderSummaryPageObj.OrderSummaryPageTitle, "Order summary");
	}
	
	public void clickMyTmobile() {
		clickElementFromMultipleSimilarElements(objOrderSummaryPageObj.ClickMyTMobileLink, "MY T-MOBILE");
	}
	
	public void clickMyTmobileSubMenu(String subMenuName) {
		clickElementFromMultipleSimilarElements(objOrderSummaryPageObj.MyTmobileSubMenu, subMenuName);
	}
	

}
