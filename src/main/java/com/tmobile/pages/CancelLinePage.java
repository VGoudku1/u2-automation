package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.CancelLinePageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class CancelLinePage extends GenericKeywords {
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	CancelLinePageObjects objCancelLinePageObj = new CancelLinePageObjects();
	
	public void selectTheLineNumber() {
		utilFunction.waitForSpinnerToDisappear(10);
		waitExplicitUntilClickable(10, objCancelLinePageObj.SelectTheLineCheckbox);
		click(objCancelLinePageObj.SelectTheLineCheckbox);
	}
	
	public void cancellation(String cancelReason) {
		waitExplicitUntilClickable(10, objCancelLinePageObj.CancelReasonDropdown);
		utilFunction.selectTmoDropdown(objCancelLinePageObj.CancelReasonDropdown, cancelReason);
	}
	
	public void clickContinueCancellLine() throws InterruptedException {
		click(objCancelLinePageObj.CancelContinueButton);
		Thread.sleep(2000);
	}
	
	public void clickTmoButtonWithText(String buttonText) throws InterruptedException {
		utilFunction.clickTmoButtonWithText(buttonText);
		Thread.sleep(2000);
	}
	
	public void verifyCancelSuccess() {
		
	}
	
	public void cancelOrderGenerated() {
		
	}

}
