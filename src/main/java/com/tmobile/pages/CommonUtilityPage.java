package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.excel.ExcelReader;
import com.tmobile.pagesObjectRepository.CommonUtilityPageObjects;

public class CommonUtilityPage extends GenericKeywords{
	
	CommonUtilityPageObjects objCommonUtilityPageObj = new CommonUtilityPageObjects();
	
	public void clickOnButtonWithAriaLabelAndText(String buttonAriaLabel) {
		clickElementFromMultipleSimilarElements(objCommonUtilityPageObj.buttonWithAriaLabel(buttonAriaLabel), buttonAriaLabel);
	}
	
	public void clickOnButtonWithAriaLabel(String buttonAriaLabel) {
		click(objCommonUtilityPageObj.buttonWithAriaLabel(buttonAriaLabel));
	}
	
	public void selectRadioButtonWithText(String radioButtonText) {
//		click(objCommonUtilityPageObj.buttonWithAriaLabel(buttonAriaLabel));
	}
	
	

}
