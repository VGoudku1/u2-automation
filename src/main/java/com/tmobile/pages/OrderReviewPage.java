package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.OrderReviewPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class OrderReviewPage extends GenericKeywords {
	OrderReviewPageObjects objOrderReviewPageObj = new OrderReviewPageObjects();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	
	public void scrollTermsAndConditions() throws InterruptedException {
		click(objOrderReviewPageObj.TermsAndContitionsPara);
		pageScrollDown();
		String scrollQuery = "document.querySelector('div.scroll-box-content').scrollTop=500";
		utilFunction.runJavaScriptQuery(scrollQuery);
		click(objOrderReviewPageObj.TermsAndContitionsPara);
	}
	
	public void acceptAndPlaceOrder()throws InterruptedException {
		click(objOrderReviewPageObj.AcceptAndPlaceOrder);
		Thread.sleep(50000);
//		utilFunction.waitForSpinnerToDisappear(2);
	}

}
