package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.LineLandingPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class LineLandingPage extends GenericKeywords {
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	LineLandingPageObjects objLineLandingPageObj = new LineLandingPageObjects();

	public void reportLost() {
		click(objLineLandingPageObj.ReportLostStolenLink);
	}

	public void clickLineDetailsOption(String option) {
		click(objLineLandingPageObj.lineDetailsOption(option));
	}

	public void expandActionHeader(String expandHeader) {
		waitExplicitUntilClickable(10, objLineLandingPageObj.ExpandRepActions);
		clickElementFromMultipleSimilarElements(objLineLandingPageObj.ExpandRepActions, expandHeader);
	}

	public void clickChangePlanSevicesButton() {
		waitExplicitUntilClickable(10, objLineLandingPageObj.ChangePlanServicesButton);
		click(objLineLandingPageObj.ChangePlanServicesButton);
	}

	public void clickChangePlanSevicesButtonWithText(String buttonText) {
		waitExplicitUntilClickable(10, objLineLandingPageObj.ChangePlanServicesButton);
		clickElementFromMultipleSimilarElements(objLineLandingPageObj.ChangePlanServicesButton, buttonText);
	}

	public void clickChangeNumberButton() {
		waitExplicitUntilClickable(10, objLineLandingPageObj.ExpandLineDevice);
		click(objLineLandingPageObj.ExpandLineDevice);
	}

	public void clickOnDemandPassButton() {
		waitExplicitUntilClickable(10, objLineLandingPageObj.OnDemandPassButton);
		click(objLineLandingPageObj.OnDemandPassButton);
	}

	public void clickExpandDataHeader() {
		waitExplicitUntilClickable(10, objLineLandingPageObj.ExpandData);
		click(objLineLandingPageObj.ExpandData);
	}

	public void selectDemandPassWithName(String passName) {
		click(objLineLandingPageObj.selectDemandPass(passName));
	}

	public void selectNoOfPassesQuantity(int quantity) {
		String newQuantity = Integer.toString(quantity);
		utilFunction.selectTmoDropdown(objLineLandingPageObj.OnDemandPassQuantuty, newQuantity);
	}

	public void clickSetOrderDate() {
		click(objLineLandingPageObj.SetOrderDateButton);
	}

}
