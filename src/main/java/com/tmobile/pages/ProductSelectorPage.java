package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.ProductSelectorPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class ProductSelectorPage extends GenericKeywords {
	ProductSelectorPageObjects objProductSelectorPageObj = new ProductSelectorPageObjects();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	public void selectPlan() throws InterruptedException {
		waitExplicitUntilInvisible(10, objProductSelectorPageObj.VisibleSpinner);
		String isHeaderExpanded = getAttribute(objProductSelectorPageObj.ConectTMobilePlanHeader, "aria-expanded");
		if(isHeaderExpanded.equalsIgnoreCase("false")) {
			clickElementFromMultipleSimilarElements(objProductSelectorPageObj.PlanHeader, "Heritage Mobile Internet Plans");
			clickElementFromMultipleSimilarElements(objProductSelectorPageObj.PlanHeader, "Connect by T-Mobile");	
		}
//		clickElementFromMultipleSimilarElements(objProductSelectorPageObj.PlanHeader, "Connect by T-Mobile");
		pageScrollDown();
		String planName = getTestData().get("Selection of Plans");
		System.out.println("Plan to select: " + planName);
		this.selectConnectTmobilePlan(planName);
	}

	public void selectConnectTmobilePlan(String planName) throws InterruptedException {
		switch (planName) {
		case "ConnectTMobile_$10":
			click(objProductSelectorPageObj.Dollar_10_Plan);
			utilFunction.waitForSpinnerToDisappear(15);
			break;
		case "ConnectTMobile_$15":
			scrollToAnElement(objProductSelectorPageObj.Dollar_15_Plan);
			click(objProductSelectorPageObj.Dollar_15_Plan);
			utilFunction.waitForSpinnerToDisappear(15);
			break;

		case "ConnectTMobile_$25":
			scrollToAnElement(objProductSelectorPageObj.Dollar_25_Plan);
			click(objProductSelectorPageObj.Dollar_25_Plan);
			utilFunction.waitForSpinnerToDisappear(15);
			break;

		case "ConnectTMobile_$35":
			scrollToAnElement(objProductSelectorPageObj.Dollar_35_Plan);
			click(objProductSelectorPageObj.Dollar_35_Plan);
			utilFunction.waitForSpinnerToDisappear(15);
			break;
		}

	}
	
	public void clickGotoCartButton() {
		waitExplicitUntilClickable(5, objProductSelectorPageObj.GoTOCartButton);
		click(objProductSelectorPageObj.GoTOCartButton);

	}

	public void clickAddressValidatorContinue() {
		waitExplicitUntilClickable(5, objProductSelectorPageObj.AddressValidatorCountinue);
		click(objProductSelectorPageObj.AddressValidatorCountinue);

	}

	public void enterUserPIN(String pinToEnter) {
		input(objProductSelectorPageObj.EnterPIN, pinToEnter);
		input(objProductSelectorPageObj.ConfrimPIN, pinToEnter);
	}
	
	public void enterFirstName(String firstName) {
		input(objProductSelectorPageObj.FirstName, firstName);
	}
	
	public void enterLastName(String lastName) {
		input(objProductSelectorPageObj.LastName, lastName);
	}
	public void enterAddressLine1(String addressLine1) {
		input(objProductSelectorPageObj.AddressLine1, addressLine1);
		click(objProductSelectorPageObj.AddressLine2);
		waitExplicitUntilInvisible(10, objProductSelectorPageObj.VisibleSpinner);
	}
	
	public void pickSuggestedAddress() {
		click(objProductSelectorPageObj.SuggestedAddress);
		click(objProductSelectorPageObj.AddressValidatorCountinue);
	}
	
	public void enterConfirmPin(String confirmPinToEnter) {
		input(objProductSelectorPageObj.ConfrimPIN, confirmPinToEnter);
	}

}
