package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.LoginPageObjects;

public class LoginPage extends GenericKeywords {
	
	LoginPageObjects objLoginPageObj = new LoginPageObjects();
	
	public void lauchbrowserAndEnterUrl() {
		String env = getTestData().get("Environment");
		if(env.equalsIgnoreCase("qlab02")) {
			navigate(suiteProp.getProperty("qlab02"));
		}else if(env.equalsIgnoreCase("qlab03")){
			navigate(suiteProp.getProperty("qlab03"));
		}
	}

	public void enterDetails() throws InterruptedException {
		input(objLoginPageObj.OktaSignInUserName, getTestData().get("UserName"));
		click(objLoginPageObj.OktaSignInSubmit);
		waitExplicitUntilPresence(3, objLoginPageObj.OktaSignInPassword);
		input(objLoginPageObj.OktaSignInPassword, getTestData().get("Password"));
		
	}

	public void submitCreds() {
		click(objLoginPageObj.OktaSignInVerify);
	}
	
	public void verifyLoginSucces() throws InterruptedException {
		waitTillPageLoads();
		String url = getUrl();
//		assertEquals(url, "https://qlab02.rbl.corporate.t-mobile.com/care/home");
	}
	
	public void selectRole(String roleToSelect) {
		click(objLoginPageObj.SignInRole);
		waitExplicitUntilPresence(3, objLoginPageObj.SignInRoleItem);
		clickElementFromMultipleSimilarElements(objLoginPageObj.SignInRoleItem, roleToSelect);
	}
	
	public void clickContinueAsRole() {
		waitExplicitUntilClickable(6, objLoginPageObj.ClickContinueButton);
		click(objLoginPageObj.ClickContinueButton);
	}

}
