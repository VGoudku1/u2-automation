package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.LineSetupPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class LineSetupPage extends GenericKeywords{
	
	LineSetupPageObjects objLineSetupPageObj= new LineSetupPageObjects();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	
	public void clickGetNewNumber() {
		waitExplicitUntilClickable(6, objLineSetupPageObj.GetNewNumberRadioButton);
		click(objLineSetupPageObj.GetNewNumberRadioButton);
	}
	
	public void enterAreaCode() {
		click(objLineSetupPageObj.EnterAreaCodeInput);
		input(objLineSetupPageObj.EnterAreaCodeInput,"720");
		click(objLineSetupPageObj.SearchAreaCode);
		utilFunction.waitForElementToDisappear(objLineSetupPageObj.InlineSpinner);
		
	}
	
	public void reserveThisNumber() {
		waitExplicitUntilClickable(6, objLineSetupPageObj.GetNewNumberRadioButton);
		click(objLineSetupPageObj.GetNewNumberRadioButton);
	}

}
