package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.ShippingPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class ShippingPage extends GenericKeywords{

	ShippingPageObjects objShippingPageObj = new ShippingPageObjects();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	
	public void enterFirstName(String firstName) {
		utilFunction.enterTmoInput(null, firstName);
	}
	
	public void enterLastName(String lastName) {
		utilFunction.enterTmoInput(null, lastName);
	}
	
	public void enterAddressLine1(String addressLine) {
		input(objShippingPageObj.Address1, addressLine);
	}
	
	public void enterZipCode(String zipCode) {
		input(objShippingPageObj.EnterZipCode, zipCode);
	}
	
	public void enterCity(String enterCity) {
		click(objShippingPageObj.EnterCity);
//		input(objShippingPageObj.EnterCity, enterCity);
		utilFunction.waitForSpinnerToDisappear(10);
	}
	
	
}
