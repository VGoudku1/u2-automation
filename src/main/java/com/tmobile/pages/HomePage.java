package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.HomePageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class HomePage extends GenericKeywords {
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	HomePageObjects objHomePgeObj = new HomePageObjects();
	public void clickNavigationMenu(String MenuName) throws InterruptedException {
		waitTillPageLoads();
		clickElementFromMultipleSimilarElements(objHomePgeObj.ClickNavigationMenuItem, MenuName);
	}
	
	public void clickNavigationSubMenu(String SubMenuName) throws InterruptedException {
		waitTillPageLoads();
		waitExplicitUntilPresence(3, objHomePgeObj.ClickNavigationSubMenuItem);
		clickElementFromMultipleSimilarElements(objHomePgeObj.ClickNavigationSubMenuItem, SubMenuName);
	}

	public void clickLookupOption() {
		click(objHomePgeObj.NavBarLookup);
	}
	
	public void clickAccountOption() {
		click(objHomePgeObj.NavBarAccounts);
	}
	

	public void clickOrderLookupOption() throws InterruptedException {
		click(objHomePgeObj.OrderLookupLink);
	}

	public void enterOrderNumberToLookup(String orderNumber) {
		input(objHomePgeObj.OrderNumberInput,orderNumber);
	}

	public void clickSearchOrderNumberButton() {
		click(objHomePgeObj.OrderSearchButton);
	}
	
	public void clickSearchedOrderResult() {
		click(objHomePgeObj.ClickOrderResult);
		
	}
	
	public void clickByPass() throws InterruptedException {
		Thread.sleep(3000);
		waitExplicitUntilClickable(10, objHomePgeObj.ClickByPassOption);
		click(objHomePgeObj.ClickByPassOption);
//		utilFunction.waitForSpinnerToDisappear(10);
	}

}
