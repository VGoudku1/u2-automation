package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.PaymentPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class PaymentPage extends GenericKeywords {

	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	PaymentPageObjects objPaymentPageObj = new PaymentPageObjects();
	public void editPayemntButton() {
		waitExplicitUntilClickableWithException(30,objPaymentPageObj.EditPayment);
		click(objPaymentPageObj.EditPayment);
	}
	
	public void enterCardDetails() {
		input(objPaymentPageObj.EnterName, getTestData().get("CardHolderName"));
		input(objPaymentPageObj.EnterCardNumber, getTestData().get("CardNumber"));
//		utilFunction.waitForSpinnerToDisappear(3);
		utilFunction.selectTmoDropdown(objPaymentPageObj.ExpirationMonth, getTestData().get("CardExpirationMonth"));
//		click(objPaymentPageObj.ExpirationMonth);
//		clickElementFromMultipleSimilarElements(objPaymentPageObj.ExpirationMonthList, getTestData().get("CardExpirationMonth"));
		click(objPaymentPageObj.ExpirationYear);
		clickElementFromMultipleSimilarElements(objPaymentPageObj.ExpirationYearList, getTestData().get("CardExpirationYear"));
		input(objPaymentPageObj.EnterCvv, getTestData().get("CVV"));
		click(objPaymentPageObj.ClickDoneButton);
	}
	
	public void enterCreditCardAddress() {
		input(objPaymentPageObj.AddressLine1, getTestData().get("Address"));
		input(objPaymentPageObj.EnterZipCode, getTestData().get("ZipCode"));
		click(objPaymentPageObj.EnterCity);
		utilFunction.waitForSpinnerToDisappear(10);
	}
	
	public void checkTermsAndConditions() {
		click(objPaymentPageObj.CheckTnC);
	}
	
	public void clickContinuePaymentPage() {
		click(objPaymentPageObj.ClickContinue);
		utilFunction.waitForSpinnerToDisappear(10);
	}
	
}
