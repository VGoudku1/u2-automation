package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.ChangeSimPageObjects;

public class ChangeSimPage extends GenericKeywords{
	
	ChangeSimPageObjects objChangeSimPageObj = new ChangeSimPageObjects();
	
	public void clickChangeSim() {
		click(objChangeSimPageObj.SubmitButtonChangeSIM);
	}

}
