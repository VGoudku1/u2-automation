package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.SupportLandingPageObjects;

public class SupportLandingPage extends GenericKeywords {
	
	SupportLandingPageObjects objSupportLandingPageObj = new SupportLandingPageObjects();
	
	public void clickSupportLinkWithText(String linkTextToClick) {
		clickElementFromMultipleSimilarElements(objSupportLandingPageObj.SupportLinks, linkTextToClick);
	}
	
	public void expandDeviceOptionsHeader(String headerText) {
		System.out.println("Expanding hearedr: "+ headerText);
		clickElementFromMultipleSimilarElements(objSupportLandingPageObj.DeviceOptiosHeader, headerText);
	}
	
	public void clickUnBlockSubOptions(String subOptions) {
		System.out.println("Expanding hearedr: "+ subOptions);
		clickElementFromMultipleSimilarElements(objSupportLandingPageObj.UnblockSubOptions, subOptions);
	}
	
	public void clickRemoveFromBlock() {
		click(objSupportLandingPageObj.ClickRemoveButton);
	}
	

}
