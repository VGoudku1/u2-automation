package com.tmobile.pages;

import static org.testng.Assert.assertEquals;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.BringYourDevicePageObjects;
import com.tmobile.pagesObjectRepository.HomePageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class BringYourDevicePage extends GenericKeywords {
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	BringYourDevicePageObjects objBringYourDevicePageObj = new BringYourDevicePageObjects();
	HomePageObjects objHomePgeObj = new HomePageObjects();
	
	public void enterZipCode(String zipCode) throws InterruptedException {
		waitExplicitUntilPresence(3, objHomePgeObj.ClickNavigationSubMenuItem);
		input(objBringYourDevicePageObj.EnterZipCode, zipCode);

	}

	public void clickZipCountinueButton() {
		waitExplicitUntilClickable(6, objBringYourDevicePageObj.zipContinueButton);
		waitExplicitUntilPresence(3, objBringYourDevicePageObj.zipContinueButton);
		click(objBringYourDevicePageObj.zipContinueButton);

	}

	public void enterIEMEINumber(String IEMEINumber) {
		input(objBringYourDevicePageObj.EnterIEMEITextbox, IEMEINumber);
	}

	public void clickCompatibilityButton() {
		click(objBringYourDevicePageObj.CheckCompatibilityButton);
		utilFunction.waitForSpinnerToDisappear(10);
	}

	public void verifyValidIEMEI() {
		waitExplicitUntilInvisible(10, objBringYourDevicePageObj.DeviceValidationSpinner);
		boolean textTrue = verifyExactTextPresent(objBringYourDevicePageObj.ValidIEMEIText, "You're all set.");
		assertEquals(textTrue, true);
	}

	public void clickCloseDialog() {
		boolean isDialog = verifyElementPresent(objBringYourDevicePageObj.CloseModalButton);
		System.out.println("Valid IEMEI Dialog Present: "+ isDialog);
		if (isDialog) {
			click(objBringYourDevicePageObj.CloseModalButton);
		}

	}

	public void clickUnlockedMyPhoneCheckbox() {
		boolean isPresent = verifyElementPresent(objBringYourDevicePageObj.UnlockedPhoneCheckBox);
		click(objBringYourDevicePageObj.UnlockedPhoneCheckBox);
	}

	public void enterSimNumber(String SimNumber) {
		input(objBringYourDevicePageObj.EnterSimNumberTextBox, SimNumber);
//		utilFunction.waitForSpinnerToDisappear();
	}

	public void entereSIMmNumber(String SimNumber) {
		input(objBringYourDevicePageObj.EntereSIMNumberTextBox, SimNumber);
//		utilFunction.waitForSpinnerToDisappear();
	}

	public void selectUseMyeSIMButton() {
		click(objBringYourDevicePageObj.SelectUseMyeSIMCard);
		utilFunction.waitForSpinnerToDisappear(10);
	}

	public void selectUseMySimButton() {
		click(objBringYourDevicePageObj.SelectUseMySimCard);
		utilFunction.waitForSpinnerToDisappear(10);
	}

	public void selectSimYouNeedButton() {
		click(objBringYourDevicePageObj.SelectSimCardYouNeed);
//		utilFunction.waitForSpinnerToDisappear(10);
	}

	public void portToPrepaid() {
		boolean modalDisplayed = verifyElementPresent(objBringYourDevicePageObj.PortPrepaidContinueButton);
		if (modalDisplayed) {
			click(objBringYourDevicePageObj.PortPrepaidContinueButton);
		}
	}

	public void clickChoosePlanButton() {
		click(objBringYourDevicePageObj.ChoosePlan);
	}

}
