package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.LostStolenDevicePageObjects;

public class LostStolenDevicePage extends GenericKeywords {

	LostStolenDevicePageObjects objLostStolenDevicePageObj = new LostStolenDevicePageObjects();
	public void clickYesSuspendRadioButton() {
		click(objLostStolenDevicePageObj.YesSuspendRadioButton);

	}

	public void clickYesBlockDeviceRadioButton() {
		click(objLostStolenDevicePageObj.YesBlockDeviceRadioButton);
	}
	
	public void clickAcceptTnC() {
		click(objLostStolenDevicePageObj.AcceptTncCheckbox);
	}
	
	public void submitSuspendRequest() {
		click(objLostStolenDevicePageObj.ClickSubmitButton);
	}
	
	public void clickBlockMyIMEIButton() {
		clickElementFromMultipleSimilarElements(objLostStolenDevicePageObj.ClickBlockIMEIButton, "Block my IMEI");
	}

}
