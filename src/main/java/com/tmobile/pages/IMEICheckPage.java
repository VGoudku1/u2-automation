package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.IMEICheckPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class IMEICheckPage extends GenericKeywords {

	IMEICheckPageObjects objIMEICheckPageObj = new IMEICheckPageObjects();
	CommonUtilFunctions utilFunctions = new CommonUtilFunctions();
	public void enterIMEINumber(String ImeiNumber) {
		input(objIMEICheckPageObj.EnterIMEITextBox, ImeiNumber);
	}
	
	public void clickSearchButton() {
		click(objIMEICheckPageObj.SearchIMEIButton);
	}
	
	public void selectBlockIMEIReason(String dropDownReason) {
		utilFunctions.selectTmoDropdown(objIMEICheckPageObj.SelectBlockReason, dropDownReason);
	}
	
	public void addBlockButton() {
		click(objIMEICheckPageObj.AddBlockButton);
	}
}
