package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.RatePlansPageObjects;
import com.tmobile.utils.CommonUtilFunctions;

public class RatePlanPage extends GenericKeywords {
	
	RatePlansPageObjects objRatePlansPageObj = new RatePlansPageObjects();
	CommonUtilFunctions utilFunctions = new CommonUtilFunctions();
	public void select25DollarConnectTmobilePlan() {
		click(objRatePlansPageObj.Dollar25PLan);
		utilFunctions.waitForSpinnerToDisappear(10);
		
	}

}
