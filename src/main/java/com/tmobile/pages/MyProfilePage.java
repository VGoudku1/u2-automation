package com.tmobile.pages;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pagesObjectRepository.MyProfilePageObjects;

public class MyProfilePage extends GenericKeywords{
	
	MyProfilePageObjects objMyProfilePageObj = new MyProfilePageObjects();
	
	public void expandMyProfileHeader(String profileHeader) {
		clickElementFromMultipleSimilarElements(objMyProfilePageObj.MyProfileHeader, profileHeader);
	}
	
	public void editProfileInformationWithName(String infoHeader) {
		click(objMyProfilePageObj.editProfileInformation(infoHeader));
	}
	
	public void saveEditedProfileInfo() {
		click(objMyProfilePageObj.SaveProfileInfoButton);
	}
	
	public void verifyChangesSaved() {
		verifyExactTextPresent(objMyProfilePageObj.ChangesSaved, "Changes saved.");
	}
	
	public void enterAccountEmail(String email) {
		input(objMyProfilePageObj.AccountEmailTextBox,email);
	}
	
	public void retypeAccountEmail(String retypeEmail) {
		input(objMyProfilePageObj.RetypeAccountEmailTextBox,retypeEmail);
		
	}

}
