package com.tmobile.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.HtmlEmail;

/*
 * This is to send the Automation status report in Email.
 * Developer: HariVenkata Lakshmi Kumar, Mynampati
 * Created date: 6/12/2019
 * 
 * Copied from previous project & reusing here.
 * Updated date: 2/24/2020
 */

public class SendMail {
	private static final String HOST = "smtp.verizon.com";
	private static final int PORT = 25;

	public static void main(String[] args) throws IOException {
		SendMail sender = new SendMail();
		String OS = System.getProperty("os.name").toLowerCase();
		String path = System.getProperty("user.dir");
		String seperator = "/";
		if (OS.indexOf("win") >= 0) {
			seperator = "\\";
		}

		String filePath = path + seperator + "Report_Status" + ".html";
		System.out.println(filePath);
		sender.sendSimpleEmail(filePath);
	}

	public void sendSimpleEmail(String reportfilepath) throws IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/YYYY");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date()); // Now use today date.
		String timeStamp = sdf.format(c.getTime());
		//from address
		String fromAddress = "vbg.e2e.automation@verizon.com";
		
		//to addresses
		String toAddress[] = { "amar-directs@verizon.com" };
		
		//email subject
		String subject = "SFDC-Consumer Automation Status Report" + " " + timeStamp;
		
		//email body
		InputStream inputStream = new FileInputStream(reportfilepath);
		StringWriter writer = new StringWriter();
		IOUtils.copy(inputStream, writer, "UTF-8");
		String message = writer.toString();
		
		//set all email parameters and send email
		try {
			Email email = new HtmlEmail();
			email.setHostName(HOST);
			email.setSmtpPort(PORT);
			email.setFrom(fromAddress);
			email.setSubject(subject);
			email.setMsg(message);
			email.addTo(toAddress);
			email.send();
		} catch (Exception ex) {
			System.out.println("Unable to send email");
			System.out.println(ex);
		}
	}
}
