package com.tmobile.utils;

import java.util.Base64;
import java.util.Scanner;

public class PasswordProtect {
	
	public String encryptPass(String message, String secretKey){       
		try {
		        if (message==null || secretKey==null ) return null;       
		        char[] keys=secretKey.toCharArray();
		        char[] mesg=message.toCharArray();           
		        int ml=mesg.length;
		        int kl=keys.length;
		        char[] newmsg=new char[ml];           
		        for (int i=0; i<ml; i++){
		       newmsg[i]=(char)(mesg[i]^keys[i%kl]);
		        }
		        mesg=null;
		        keys=null;
		        return new String(Base64.getEncoder().encode(new String(newmsg).getBytes()));
		} catch (Exception e) {
		        e.printStackTrace();
		        return null;
		    }
		}
	
	public String decryptPass(String message, String secretKey){
	    try {
	      if (message==null || secretKey==null ) return null;         
	      char[] keys=secretKey.toCharArray();
	      char[] mesg=new String(Base64.getDecoder().decode(message)).toCharArray();
	      int ml=mesg.length;
	      int kl=keys.length;
	      char[] newmsg=new char[ml];
	      for (int i=0; i<ml; i++){
	        newmsg[i]=(char)(mesg[i]^keys[i%kl]);
	      }
	      mesg=null; keys=null;
	      return new String(newmsg);
	    }
	    catch ( Exception e ) {
	      return null;
	        } 
	      }

	

	public static void main(String[] args) {
		PasswordProtect pp=new PasswordProtect();
		Scanner in = new Scanner(System.in);
		System.out.println("Enter your password:");
		String passWord = in.next();
		in.close();
		String encryptPass= pp.encryptPass(passWord, "Automation");
		System.out.println("Your encrypted password is : "+encryptPass);	
		//GenericKeywords g = new GenericKeywords();
		
	}
	
	

}
