package com.tmobile.utils;

import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.excel.ExcelReader;

public class CommonUtilFunctions extends GenericKeywords{	
	String testDataExcel = System.getProperty("user.dir")+"\\data\\TestDataSheet.xlsx";
	ExcelReader excel = new ExcelReader(testDataExcel);
	public String TMOButtonText = null;
	
	
	/**
	 * This is to run a bat file to do following actions
	 * Delete chrome webdriver(s) in background
	 * Move a copy of report to project location
	 * delete extra created images files in report folder
	 * @author Vikram
	 */
	public void runBatFile(String fileLocation, String fileName){
		ProcessBuilder pb = new ProcessBuilder(fileLocation+"\\"+fileName+".bat");
		pb.redirectError();
		try {
			Process p = pb.start();
			try (InputStream inputStream = p.getInputStream()) {
				@SuppressWarnings("unused")
				int in = -1;
				while ((in = inputStream.read()) != -1) {}
			}
			p.waitFor();
		} catch (IOException | InterruptedException ex) {
			ex.printStackTrace();
		}		
	}
	
	/**
	 * This method click on button with parent tag as tmo-button with certain text
	 * @param the text of the tmo-button
	 * @author Vikarm
	 */
	public void clickTmoButtonWithText(String buttonText) {
    	this.TMOButtonText = buttonText;
    	////tmo-button//button[text()='Continue'][not(@disabled)]
    	By TMOButtonLocator = By.xpath("//tmo-button//button[text()='"+this.TMOButtonText+"'][not(@disabled)]");
    	System.out.println("Clicking TMO Button: "+ getText(TMOButtonLocator) );
//    	waitExplicitUntilClickable(7, TMOButtonLocator);
    	click(TMOButtonLocator);
	}
	
	/**
	 * This method selects the value by text from element tmo-dropdown
	 * @param the text of the tmo-dropdown to be selected
	 * @author Vikarm
	 */
	public void selectTmoDropdown(By tmoDropdown, String dropDownText) {
		waitExplicitUntilClickable(3, tmoDropdown);
    	click(tmoDropdown);
    	By dropDownItems = tmoDropdown.cssSelector("li span");
    	waitExplicitUntilClickable(3, dropDownItems);
    	clickElementFromMultipleSimilarElements(dropDownItems, dropDownText);
	}
	
	/**
	 * This method selects the value by text from element tmo-dropdown
	 * @param the text of the tmo-dropdown to be selected
	 * @author Vikarm
	 */
	public void enterTmoInput(By tmoInput, String inputText) {
		waitExplicitUntilClickable(3, tmoInput);
    	By inputTextBox = tmoInput.cssSelector("input");
//    	clear(inputTextBox);
    	waitExplicitUntilClickable(3, inputTextBox);
    	input(inputTextBox, inputText);
	}
	
	/**
	 * This method selects the radio button by text from element tmo-radio-button
	 * @param the text of the tmo-radio-button to be selected
	 * @author Vikarm
	 */
	public void selectTmoRadioButtonWithText(String radioButtonText) {
    	By radioButtonCss = By.cssSelector("tmo-radio-button label span ~ p");
    	waitExplicitUntilClickable(3, radioButtonCss);
    	clickElementFromMultipleSimilarElements(radioButtonCss, radioButtonText);
	}
	
	/**
	 * This method selects the radio button by text from element tmo-radio-button
	 * @param the text of the tmo-radio-button to be selected
	 * @author Vikarm
	 */
	public void clickTmoCheckBoxWithText(String checkBoxText) {
    	By checkBoxCss = By.cssSelector("tmo-checkbox label span ~ p p");
    	waitExplicitUntilClickable(3, checkBoxCss);
    	clickElementFromMultipleSimilarElements(checkBoxCss, checkBoxText);
	}
	
	/**
	 * This is to run a bat file to do following actions
	 * Delete chrome webdriver(s) in background
	 * Move a copy of report to project location
	 * delete extra created images files in report folder
	 * @author Vikram
	 */
	public void waitForSpinnerToDisappear(int secondsToWait) {
		By VisibleSpinner = By.xpath("//tmo-spinner/div[not(@hidden)]//i");
		boolean isDisplayed = getDriver().findElement(VisibleSpinner).isDisplayed();
		System.out.println("Spinner Displayed: "+ isDisplayed);
		if(isDisplayed) {
		waitExplicitUntilInvisible(secondsToWait, VisibleSpinner);
		}
	}
	
	/**
	 * This is to run a bat file to do following actions
	 * Delete chrome webdriver(s) in background
	 * Move a copy of report to project location
	 * delete extra created images files in report folder
	 * @author Vikram
	 */
	public void waitForElementToDisappear(By elemntLocator) {
		boolean isDisplayed = isElementPresent(elemntLocator);
		System.out.println("Element Displayed: "+ isDisplayed);
		if(isDisplayed) {
		waitExplicitUntilInvisible(10, elemntLocator);
		}
	}
	//document.querySelector('div.scroll-box-content').scrollTop=500
	public void runJavaScriptQuery(String jQuery) {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript(jQuery);
	}
	
	/**
	 * This methods is to write the order number into excel file
	 * It will find the currently executing scenario name in excel file 1st Column
	 * If the match is found add the order number in 26th column
	 * @author Vikram
	 * @param orderNumber - generated on web page
	 * @throws IOException 
	 */
	public void writeOrderNumberToExcel(String orderNumber) throws IOException {
		XSSFSheet sheetName = excel.getSheetFromExcel("HomePage");
		String current_scenario =  getTestData().get("Scenario");
		int rows = excel.getRowCount(sheetName.getSheetName());
		for(int i=0;i<=rows;i++) {
			if(excel.getCellData(sheetName.getSheetName(), "Scenario", i).equalsIgnoreCase(current_scenario)) {
				excel.setCellData(orderNumber, sheetName.getSheetName(), i, 26);
				excel.writeDataToExcel(testDataExcel);
				break;
			}
		}
	}
	
	/**
	 * This methods is to reads the order number into excel file
	 * It will find the currently executing scenario name in excel file 1st Column
	 * If the match is found read the order number in 26th column
	 * @author Vikram
	 * @param orderNumber - generated on web page
	 * @throws IOException 
	 */
	public String readOrderNumberFromExcel() throws IOException {
		String orderNumber = null;
		XSSFSheet sheetName = excel.getSheetFromExcel("HomePage");
		String current_scenario =  getTestData().get("Scenario");
		int rows = excel.getRowCount(sheetName.getSheetName());
		for(int i=0;i<=rows;i++) {
			if(excel.getCellData(sheetName.getSheetName(), "Scenario", i).equalsIgnoreCase(current_scenario)) {
				orderNumber =  excel.getCellData(sheetName.getSheetName(), "GeneratedOrders", i);
				break;
			}
		}
		return orderNumber;
	}
	
	/**
	 * This methods is to write the order number into excel file
	 * It will find the scenario name passed in parameter in 1st column of excel file
	 * If the match is found add the order number in 25th column
	 * @author Vikram
	 * @param orderNumber - generated on web page
	 * @throws IOException 
	 */
	public void passTheOrderNumber(String scenarioName, String orderNumber) throws IOException {
		XSSFSheet sheetName = excel.getSheetFromExcel("HomePage");
		int rows = excel.getRowCount(sheetName.getSheetName());
		for(int i=0;i<=rows;i++) {
			if(excel.getCellData(sheetName.getSheetName(), "Scenario", i).equalsIgnoreCase(scenarioName)) {
				excel.setCellData(orderNumber, sheetName.getSheetName(), i, 25);
				excel.writeDataToExcel(testDataExcel);
				break;
			}
		}
		
	}

}
