package com.tmobile.test.executor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.excel.ExcelReader;

import io.cucumber.testng.CucumberFeatureWrapper;
import io.cucumber.testng.PickleEventWrapper;
import io.cucumber.testng.TestNGCucumberRunner;

public class TestExecutionEngine extends GenericKeywords{
	
	private TestNGCucumberRunner testNGCucumberRunner;
	
	/**collection to add testdata for all executing scenarios*/
	static HashMap<String, ArrayList<HashMap<String, String>>> allExecutingScenariosTestData= new LinkedHashMap<>();

	/**collection to add scenario(s) names and used to fetch correct testdata from 'allExecutingScenariosTestData' HashMap*/
	HashMap<String, Integer> scenarioDetails = new LinkedHashMap<>();


	/**
	 * 	Function to create instance for 'TestNGCucumberRunner' class
	 * @author Vikram
	 */
	@BeforeClass(alwaysRun = true)
	public void setUpClass() {
		try{
			testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
		}catch (Exception e) {
			e.printStackTrace();
			logInfo("ERROR",  "Error while executing method " + getMethodName() +" in the class file "+getClassName()+ " with error: " + e.toString());
		} 
	}


	/**
	 * Function to pass testdata to step definition file,initialize reporter and run scenario(s) based on input from 'scenarios' Function
	 * @param pickleWrapper
	 * @param featureWrapper
	 * @author Vikram
	 */
	@Test(dataProvider = "scenarios")
	public void runScenario(PickleEventWrapper pickleWrapper, CucumberFeatureWrapper featureWrapper)  {
		try{
			//scenario name and testdata for executing scenario from the 'allExecutingScenariosTestData' collection
			String currentScenarioName = pickleWrapper.getPickleEvent().pickle.getName().trim();
			if(rerun)
				System.out.println("Rerun execution : "+currentScenarioName);
			else
				System.out.println("primary execution : "+currentScenarioName);
			
			if(scenarioDetails.containsKey(currentScenarioName)){
				scenarioDetails.put(currentScenarioName, scenarioDetails.get(currentScenarioName)+1);
			}else{
				scenarioDetails.put(currentScenarioName, 1);
			}
			
			  
			
			//Initializing the browser
//			String[] nameOfTheResource = suiteProp.getProperty("admin_username_uat").split("@")[0].split("\\.");
			String[] nameOfTheResource = suiteProp.getProperty("admin_username_uat").split("@");
			String name = "SL Automation Team: "+nameOfTheResource[0].toUpperCase()+" "+nameOfTheResource[1].toUpperCase()+" || Scenario: "+currentScenarioName;
			openBrowser(name);

			//Passing testdata to generic keywords
			threadTestData.set(allExecutingScenariosTestData.get(currentScenarioName).get((scenarioDetails.get(currentScenarioName))-1));
			
			//initiating reporter for executing scenario
//			new ExtentReportGenerator().initiateReporter(featureWrapper,currentScenarioName,rerun);

			try{
				//Executing the scenario
				testNGCucumberRunner.runScenario(pickleWrapper.getPickleEvent());
			}catch (Throwable e) {
				e.printStackTrace();
				logInfo("ERROR",  "Error while executing method " + getMethodName() +" in the class file "+getClassName()+ " with error: " + e.toString());
			}
		}
		catch (Exception e) {
			
			
			e.printStackTrace();
			logInfo("ERROR",  "Error while executing method " + getMethodName() +" in the class file "+getClassName()+ " with error: " + e.toString());
		} 
	}

	/**
	 *  Function(Dataprovider) to pass scenario(s) from execution scenarios list filtered
	 * @return Scenario
	 * @author Vikram
	 */
	@DataProvider(parallel = false)
	public Iterator<Object[]> scenarios() {
		LinkedList<Object[]> modifiedList = new LinkedList<>();
		LinkedList<Object[]> newModifiedSortedList = new LinkedList<>();
		try{
			if (testNGCucumberRunner == null) {
				return modifiedList.iterator();
			}
			modifiedList = filterTheFeature(testNGCucumberRunner.provideScenarios());
		}
		catch (Exception e) {
			e.printStackTrace();
			logInfo("ERROR",  "Error while executing method " + getMethodName() +" in the class file "+getClassName()+ " with error: " + e.toString());
		} 
		Iterator x = modifiedList.descendingIterator();
		
		return x;
	}

	/**
	 * Function to filter scenario and add to execution array based on data from Master Sheet
	 * @param data
	 * @return ArrayList of scenario(s) to execute
	 * @author Vikram
	 */
	private LinkedList<Object[]> filterTheFeature(Object[][] data) {
		LinkedList<Object[]> modifiedList = new LinkedList<>();
		LinkedList<Object[]> newModifiedSortedList = new LinkedList<>();
		HashMap<String, LinkedList<Object[]>> featureAndScenario =new LinkedHashMap<>();
		try{
			if(allExecutingScenariosTestData == null || allExecutingScenariosTestData.isEmpty()){
				System.exit(1);
			}else{
				LinkedList<String> scenarioList=null;
				if(rerun){
					scenarioList = new LinkedList<>(failedScenario);
					if(scenarioList.isEmpty() || scenarioList==null)
						System.exit(1);
				}else{
					scenarioList = new LinkedList<>(allExecutingScenariosTestData.keySet());
					System.out.println("The Scenario List here is Sorted: "+ scenarioList);
				}
				if(data != null){
					for ( String scenarioName : scenarioList) {
						scenarioName= scenarioName.trim();
						LinkedList<Object[]> tempObjectCollector = new LinkedList<>();
						String featureName="";
						for (int i = 0; i < data.length; i++) {
							featureName  = ((CucumberFeatureWrapper)data[i][1]).toString().trim().replace("\"", "");
							String scenarioNameFromFeatureFile = ((PickleEventWrapper)data[i][0]).toString().trim().replaceAll("\"", "");
							if(scenarioName.contains((scenarioNameFromFeatureFile))){
								if(rerun){
									tempObjectCollector.add(data[i]);
									if(featureAndScenario.containsKey(featureName))
										tempObjectCollector.addAll(featureAndScenario.get(featureName));
									featureAndScenario.put(featureName,tempObjectCollector);
								}else{
									
									for (int k = 0; k < allExecutingScenariosTestData.get(scenarioName).size(); k++) {
										tempObjectCollector.add(data[i]);
										System.out.println(featureAndScenario.toString()+" "+featureName+" "+featureAndScenario.containsKey(featureName));
										if(featureAndScenario.containsKey(featureName)) {
											tempObjectCollector.addAll(featureAndScenario.get(featureName));
										}
										featureAndScenario.put(featureName,tempObjectCollector);
									}
								}
								break;
							}
						}
					}
					System.out.println("Adding in Modified List: "+featureAndScenario.keySet());
					for (String key : featureAndScenario.keySet())
						modifiedList.addAll(featureAndScenario.get(key));
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			logInfo("ERROR",  "Error while executing method " + getMethodName() +" in the class file "+getClassName()+ " with error: " + e.toString());
		} 
		Iterator x = modifiedList.descendingIterator();
		while(x.hasNext()) {
			newModifiedSortedList.add((Object[]) x.next());
		}
		System.out.println("New Scenario List2: "+ newModifiedSortedList.peek());
		return newModifiedSortedList;
	}

	/**
	 * Function to put all scenario in feature file to execution list
	 * @param data
	 * @return ArrayList of scenario(s) to execute
	 * @author Vikram
	 */
	/*private ArrayList<Object[]> getFeatureList(Object[][] data) {
		ArrayList<Object[]> modifiedList = new ArrayList<>();
		try{
			if(data != null){
				for (int i = 0; i < data.length; i++) {
					modifiedList.add(data[i]);
				}
			}
		}catch (Exception e) {
			logInfo("ERROR",  "Error while executing method " + getMethodName() +" in the class file "+getClassName()+ " with error: " + e.toString());
		}
		return modifiedList;
	}*/

	/**
	 * Function to close resources opened before/while execution
	 * @author Vikram
	 */
	@AfterClass(alwaysRun = true)
	public void tearDownClass() {
		try{
			if (testNGCucumberRunner == null) {
				return;
			}
			testNGCucumberRunner.finish();
		}
		catch (Exception e) {
			logInfo("ERROR",  "Error while executing method " + getMethodName() +" in the class file "+getClassName()+ " with error: " + e.toString());
		}
	}

	/**
	 * Function to fetch com.verizon.sfdc.CucumberFiles data from excel before com.verizon.sfdc.CucumberFiles begins
	 * @author Vikram
	 */

	@BeforeSuite
	public void testData(){
		try{
			allExecutingScenariosTestData = new ExcelReader(suiteProp.getProperty("dataSheet")).masterExecutionData("Master Sheet");
		}
		catch (Exception e) {
			logInfo("ERROR",  "Error while executing method " + getMethodName() +" in the class file "+getClassName()+ " with error: " + e.toString());
		}
	}

}