package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.IMEICheckPage;

import io.cucumber.java.en.When;

public class IMEICheckPage_SD extends GenericKeywords {

	IMEICheckPage ObjIMEICheckPage = new IMEICheckPage();
	
	@When("I enter IMEI number to block")
	public void enterIMEI() throws Exception {
		String ImeiNumber = getTestData().get("IMEI Number");
		ObjIMEICheckPage.enterIMEINumber(ImeiNumber);
	}
	
	@When("I click on search IMEI button")
	public void searchButton() throws Exception {
		ObjIMEICheckPage.clickSearchButton();
	}
	
	@When("I select block IMEI reason as {string}")
	public void i_select_the_block_reason_as(String blockReason) {
		ObjIMEICheckPage.selectBlockIMEIReason(blockReason);
	}
	
	@When("Click on add IMEI to block button")
	public void addIMEINumberToBlock() {
		ObjIMEICheckPage.addBlockButton();
	}
	
}
