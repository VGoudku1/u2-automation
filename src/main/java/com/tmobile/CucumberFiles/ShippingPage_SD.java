package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.ProductSelectorPage;
import com.tmobile.pages.ShippingPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.When;

public class ShippingPage_SD extends GenericKeywords{

CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	ShippingPage objShippingPage = new ShippingPage();
	ProductSelectorPage objProductSelector = new ProductSelectorPage();
	@When("I enter shipping address for device")
	public void enterShipping_address() {
		String address = getTestData().get("Address");
		String city = getTestData().get("City");
		String zipcode = getTestData().get("ZipCode");
		objShippingPage.enterAddressLine1(address);
		objShippingPage.enterZipCode(zipcode);
		objShippingPage.enterCity(city);
		objProductSelector.pickSuggestedAddress();
	}
	
}
