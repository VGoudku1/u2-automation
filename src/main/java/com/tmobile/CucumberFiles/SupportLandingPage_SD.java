package com.tmobile.CucumberFiles;

import com.tmobile.pages.SupportLandingPage;

import io.cucumber.java.en.When;

public class SupportLandingPage_SD {
	
	SupportLandingPage objSupportLandingPage = new SupportLandingPage();
	@When("I Click on support link {string}")
	public void supportLink(String supportLinkName) throws Exception {
		objSupportLandingPage.clickSupportLinkWithText(supportLinkName);
	}
	
	@When("I expand the device options header {string}")
	public void expandServiceHeader(String headerName) throws InterruptedException {
		Thread.sleep(3000);
		objSupportLandingPage.expandDeviceOptionsHeader(headerName);
	}
	
	@When("I click ubblock sub options link {string}")
	public void clickSubOptions(String subOptionsLink) throws InterruptedException {
		Thread.sleep(3000);
		objSupportLandingPage.clickUnBlockSubOptions(subOptionsLink);
	}
	
	@When("I click remove IMEI from block")
	public void clickRemoveIMEIButton(String subOptionsLink) throws InterruptedException {
		Thread.sleep(3000);
		objSupportLandingPage.clickRemoveFromBlock();
	}

}
