package com.tmobile.CucumberFiles;

import java.util.HashSet;
import java.util.Properties;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.LoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class LoginScreen_SD extends GenericKeywords{

    public static Properties suiteProp = new Properties();
    protected static HashSet<String> failedScenario = new HashSet<>();
    protected static boolean rerun=false;
    LoginPage objLoginpage = new LoginPage();

        @Given("^I am in login screen$")
        public void openUrl() throws Exception {
            objLoginpage.lauchbrowserAndEnterUrl();

        }
        
        @When("^I enter User Details$")
        public void enterCredentials() throws InterruptedException {
            objLoginpage.enterDetails();
        }
        
        @When("^Click on Sign in button$")
        public void clickVerifyButton() {
            objLoginpage.submitCreds();

        }
                
        @When("^Select role as manager care$")
        public void selectRole() throws Exception {
            objLoginpage.selectRole(getTestData().get("Role"));

        }
        
        @When("^Click continue as role button$")
        public void clickContinueAsRole() {
            objLoginpage.clickContinueAsRole();

        }
        
        @Then("^user should be on the Rebelion Care Home page$")
        public void verifyLoginScreen() throws Exception {
            objLoginpage.verifyLoginSucces();
        }


}
