package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.LineSetupPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class LineSetupPage_SD extends GenericKeywords {
	LineSetupPage objLineSetupPage = new LineSetupPage();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	
	@When("^I click on Get a New number$")
	public void clickGetANewNumber() {
		objLineSetupPage.clickGetNewNumber();
	}
	
	@When("^I enter Area Code$")
	public void enterAreaCode() {
		objLineSetupPage.enterAreaCode();
	}
	
	@When("^User should able to Reserve this number$")
	public void reserveNumber() {
		utilFunction.clickTmoButtonWithText("Reserve this number");
	}
	
	@When("^Click on Continue button preserved number$")
	public void continueButton() {
		utilFunction.clickTmoButtonWithText("Continue");
	}

}
