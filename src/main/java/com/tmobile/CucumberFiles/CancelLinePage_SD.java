package com.tmobile.CucumberFiles;

import com.tmobile.pages.CancelLinePage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CancelLinePage_SD {
	
	CancelLinePage objCancelLinePage = new CancelLinePage();
	
	@When("^I click select the line checkbox$")
	public void selectTheLine() {
		objCancelLinePage.selectTheLineNumber();
	}
	
	@When("I select the cancellation reason as {string}")
	public void i_select_the_cancellation_reason_as(String cancellationReason) {
		objCancelLinePage.cancellation(cancellationReason);
	}
	
	@When("I click continue button to cancel line")
	public void i_click_the_continue_button_as() throws InterruptedException {
		objCancelLinePage.clickContinueCancellLine();
	}

}
