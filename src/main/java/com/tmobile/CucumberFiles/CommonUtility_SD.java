package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.CommonUtilityPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class CommonUtility_SD extends GenericKeywords {

	CommonUtilityPage ObjCommonUtilityPage = new CommonUtilityPage();
	CommonUtilFunctions utilFunctions = new CommonUtilFunctions();

	@When("I click continue button with text {string}")
	public void i_click_continue_button_with_text(String tmoButtonText) throws InterruptedException {
		Thread.sleep(5000);
		utilFunctions.clickTmoButtonWithText(tmoButtonText);
	}
	
	@Given("I Click on button with aria-lable text {string}")
	public void clickButtonWithAriaLabelText(String ariaLabelText) throws Exception {
		Thread.sleep(2000);
		ObjCommonUtilityPage.clickOnButtonWithAriaLabelAndText(ariaLabelText);
	}

	@Given("Click button with aria-lable as {string}")
	public void clickButtonWithAriaLabel(String ariaLabelText) throws Exception {
		Thread.sleep(2000);
		ObjCommonUtilityPage.clickOnButtonWithAriaLabel(ariaLabelText);
	}

	@When("I Click on radio button with text {string}")
	public void clickTmoRadioButtonWithText(String radioText) throws Exception {
		Thread.sleep(2000);
		utilFunctions.selectTmoRadioButtonWithText(radioText);
	}
	
	@When("I Click on checkbox with text {string}")
	public void clickTmoCheckBoxWithText(String checkBoxText) throws Exception {
		Thread.sleep(2000);
		utilFunctions.clickTmoCheckBoxWithText(checkBoxText);
	}
	
	@When("I scroll down the page")
	public void scrollDown() throws Exception {
		Thread.sleep(2000);
		pageScrollDown();
	}
	
	@When("Order number is passed to scenario {string}")
	public void passTheOrderNumber(String scenarioName) throws Exception {
		Thread.sleep(2000);
		String orderNumber = utilFunctions.readOrderNumberFromExcel();
		String orderNumber1 = getTestData().get("GeneratedOrders");
		System.out.println("The OrderNumber Util Function: "+ orderNumber);
		System.out.println("The OrderNumber GetTestData: "+ orderNumber1);
		utilFunctions.passTheOrderNumber(scenarioName, orderNumber);
	}

}
