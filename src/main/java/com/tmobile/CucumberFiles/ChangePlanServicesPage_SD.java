package com.tmobile.CucumberFiles;

import com.tmobile.pages.ChangePlanServicesPage;

import io.cucumber.java.en.When;

public class ChangePlanServicesPage_SD {

	ChangePlanServicesPage objChangePlanServicesPage = new ChangePlanServicesPage();
	@When("I expand the choose service header {string}")
	public void expandServiceHeader(String headerName) throws InterruptedException {
		Thread.sleep(3000);
		objChangePlanServicesPage.expandChooseServiceHeader(headerName);
	}
}
