package com.tmobile.CucumberFiles;

import com.tmobile.pages.RatePlanPage;

import io.cucumber.java.en.When;

public class RatePlanPage_SD {
	
	RatePlanPage objRatePlanPage = new RatePlanPage();
	
	@When("Select $25 plan from Connect T-Mobile section")
	public void selectPlan() throws Exception {
		objRatePlanPage.select25DollarConnectTmobilePlan();
	}

}
