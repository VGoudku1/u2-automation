package com.tmobile.CucumberFiles;

import com.tmobile.pages.ChangeMsisdnPage;

import io.cucumber.java.en.When;

public class ChangeMsisdnPage_SD {
	
	
	ChangeMsisdnPage objChangeMsisdnPage = new ChangeMsisdnPage();
	
	@When("I click on get new number button")
	public void i_click_on_get_new_number_button() {
		objChangeMsisdnPage.clickOnGetNewNumberButton();
	}

	@When("I enter zip or area code")
	public void i_enter_zip_or_area_code() {
		objChangeMsisdnPage.enterZipOrAreaCode("720");
	}

	@When("I click on get this number button")
	public void i_click_on_get_this_number_button() {
		objChangeMsisdnPage.clickGetThisButton("Get this number");
	}

	@When("I click on submit request button")
	public void i_click_on_submit_request_button() {
		objChangeMsisdnPage.clickSubmitRequest("Submit request");
	}

}
