package com.tmobile.CucumberFiles;

import com.tmobile.pages.MyProfilePage;

import io.cucumber.java.en.When;

public class MyProfilePage_SD {

	
	MyProfilePage objMyProfilePage = new MyProfilePage();
	
	@When("I expands my profile header section {string}")
	public void expandHeader(String headerName) {
		objMyProfilePage.expandMyProfileHeader(headerName);

	}
	
	@When("I click on edit profile information link {string}")
	public void editProfileInfoLink(String profileInfoToEditLink) {
		objMyProfilePage.editProfileInformationWithName(profileInfoToEditLink);
	}
	
	@When("I click save profile information button")
	public void saveProfileInfo() {
		objMyProfilePage.saveEditedProfileInfo();
	}
	
	@When("I verify changes are saved")
	public void saveSuccess() {
		objMyProfilePage.verifyChangesSaved();
	}
	
	@When("I enter account email as {string}")
	public void enterAccountEmail(String accountEmail) {
		objMyProfilePage.enterAccountEmail(accountEmail);
	}
	
	@When("I retype account email as {string}")
	public void retyprEnterAccountEmail(String retypeAccountEmail) {
		objMyProfilePage.retypeAccountEmail(retypeAccountEmail);
	}
}
