package com.tmobile.CucumberFiles;

import java.io.IOException;

import com.tmobile.pages.OrderConfirmationPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class OrderConfirmationPage_SD {

	OrderConfirmationPage ObjOrderConfirmation = new OrderConfirmationPage();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();

	@Then("^I verify order is submitted successfully$")
	public void verifyOrderSubmittedPageTitle() {
		ObjOrderConfirmation.verifyOrderSubmitted();
	}
	
	@Then("^I verify order submitted is success$")
	public void verifyOrderSuspendPageTitle() {
		ObjOrderConfirmation.verifyOrderSuccess("Success!");
	}
	
	@Then("^I verify order is cancelled successfully$")
	public void verifyOrderCancellPageTitle() {
		ObjOrderConfirmation.verifyOrderSuccess("Success!");
	}
	
	@And("^Order number is generated$")
	public void orderNumber() throws IOException {
		ObjOrderConfirmation.orderNumberGenerated();
	}
	
	@And("^Suspended order number is generated$")
	public void suspendOrderNumber() throws IOException {
		ObjOrderConfirmation.suspendOrderNumberGenerated();
	}
	
	@And("^Cancelled order number is generated$")
	public void cancelOrderNumber() throws IOException {
		ObjOrderConfirmation.cancelOrderNumberGenerated();
	}
	
	@And("^Change SIM order number is generated$")
	public void ChangeSIMOrderNumber() throws IOException {
		ObjOrderConfirmation.changeSimOrderNumberGenerated();
	}

}
