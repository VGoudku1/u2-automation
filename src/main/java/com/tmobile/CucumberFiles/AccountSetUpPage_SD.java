package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.AccountSetUpPage;
import com.tmobile.pages.ProductSelectorPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.When;

public class AccountSetUpPage_SD extends GenericKeywords {

	
	AccountSetUpPage objAccountSetUpPage = new AccountSetUpPage();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	
	@When("^Click continue with account$")
	public void continueWithAccountSetup() {
		objAccountSetUpPage.clickContinueWithAccountButton();
		utilFunction.waitForSpinnerToDisappear(10);
	}
}
