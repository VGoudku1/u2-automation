package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.PaymentPage;
import com.tmobile.pages.ProductSelectorPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.When;

public class PaymentPage_SD extends GenericKeywords{

	ProductSelectorPage objProductSelector = new ProductSelectorPage();
	PaymentPage objPaymentPage = new PaymentPage();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	
	@When("^I Edit the payment$")
	public void editPayment() {
		objPaymentPage.editPayemntButton();
	} 
	
	@When("^I enter Credit card deatils$")
	public void enterCardDetails() {
		objPaymentPage.enterCardDetails();
	}
	
	@When("^I enter Credit Card Address$")
	public void enterCreditCardAddress() {
		objPaymentPage.enterCreditCardAddress();
		objProductSelector.pickSuggestedAddress();
	}
	
	@When("^I click agree Terms and conditions checkbox$")
	public void agreedTerms() {
		objPaymentPage.checkTermsAndConditions();
	}
	
	@When("^I click continue on payment page$")
	public void clickContinueWithPayment() {
		objPaymentPage.clickContinuePaymentPage();
	}
	
	
	
}
