package com.tmobile.CucumberFiles;

import com.tmobile.pages.ChangeSimPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.Given;

public class ChangeSimPage_SD {
	
	ChangeSimPage objChangeSimPage = new ChangeSimPage();
	
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();

	@Given("^I click on change SIM submit$")
	public void submitButton() throws Exception {
		objChangeSimPage.clickChangeSim();
	}

}
