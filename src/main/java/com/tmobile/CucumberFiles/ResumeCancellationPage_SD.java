package com.tmobile.CucumberFiles;

import com.tmobile.pages.ResumeCancellationPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.When;

public class ResumeCancellationPage_SD {
	
	ResumeCancellationPage objResumeCancellationPage = new ResumeCancellationPage();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();

	@When("^I click next button$")
	public void clickNextButton_ResumeCancellationPage() throws Exception {
		objResumeCancellationPage.clickNextButton();
	}
}
