package com.tmobile.CucumberFiles;

import com.tmobile.pages.BrowsePhonesPage;

import io.cucumber.java.en.When;

public class BrowsePhonePage_SD {
	
	BrowsePhonesPage objBrowsePhonesPage = new BrowsePhonesPage();
	
	@When("I select item to ship by name {string}")
	public void selectByItemNameText(String itemName) throws InterruptedException {
		objBrowsePhonesPage.clickPhonesByName(itemName);
	}
	
	@When("I stest ship by name {string}")
	public void i_click_the_continue_button_as(String itemName) throws InterruptedException {
		objBrowsePhonesPage.clickPhonesByName(itemName);
	}

}
