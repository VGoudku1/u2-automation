package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.ProductSelectorPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.When;

public class ProductSelectorPage_SD extends GenericKeywords {
	ProductSelectorPage objProductSelector = new ProductSelectorPage();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();

	@When("^Select plan from Connect T-Mobile section$")
	public void selectPlan() throws Exception {
		objProductSelector.selectPlan();
	}

	@When("^Click on Go to cart$")
	public void addToCard() {
		objProductSelector.clickGotoCartButton();
	}

	@When("^Click on Checkout$")
	public void checkoutPlan() {
		String currentUrl = getUrl();
//		if (currentUrl.contains("qlab03")) {
//			utilFunction.clickTmoButtonWithText("Checkout");
//		} else
			utilFunction.clickTmoButtonWithText("Checkout");
	}

	@When("^Enter default user PIN")
	public void enterDefaultUserPIN() {
		objProductSelector.enterUserPIN("123456");
	}

	@When("^I enter first name$")
	public void enterFirstName() {
		String fName = getTestData().get("First Name");
		objProductSelector.enterFirstName(fName);
	}

	@When("^I enter last name$")
	public void enterLastName() {
		String lName = getTestData().get("Last Name");
		objProductSelector.enterLastName(lName);
	}

	@When("^I enter Home Address$")
	public void enterHomeAddress() {
		objProductSelector.enterAddressLine1(getTestData().get("Address"));
		objProductSelector.pickSuggestedAddress();
	}

	@When("^I eneter ZIP Code$")
	public void enterZipCodeCheckoutPage() {
		String getZip = getTestData().get("ZipCode");
	}

	@When("^Click Continue on address validator dialog$")
	public void continueButton() {
		objProductSelector.clickAddressValidatorContinue();
	}
}
