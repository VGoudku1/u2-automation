package com.tmobile.CucumberFiles;

import java.awt.Desktop;
import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.tmobile.excel.ExcelReader;
import com.tmobile.test.executor.TestExecutionEngine;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.testng.CucumberOptions;

/**
 * This class is the execution engine
 * 
 * @author Nataraaj
 */
@CucumberOptions(features = { "src/main/java/com/tmobile/CucumberFiles/featureFiles" }, 
glue = {"com.tmobile.CucumberFiles" },
plugin = {"json:target/cucumber.json"},
tags="",
monochrome=true
		)
public class TestRunnerFile extends TestExecutionEngine {

	@Parameters("ExecutionName")
	@BeforeTest
	public void executionType(String executionType) {
		if (executionType.equals("ReRunExecution"))
			rerun = true;
	}

	/**
	 * Function to call before execution of each scenario
	 *//*
		 * @BeforeMethod public void openBrowser(){ super.openBrowser(); }
		 */

	/**
	 * Function to attach screenshot to maven-cucumber report if scenario fails
	 */
	@After
	public void takeScreenShotsOnStepFailure(Scenario scenario) {
		System.out.println("Executing After Hook and the scenario name is: "+ scenario.getName());
		if(scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png", scenario.getName()); 
        } 
	}
		
	/**
	 * Function to call after execution of each scenario
	 */
	@AfterMethod
	public void closeBroswer() {
		try {
			getDriver().quit();
		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());
		}
	}

	/**
	 * Function to call after execution of all test
	 */
	@AfterSuite
	public void closeResources() {
		try {
			ExcelReader.book.close();
			System.out.println();
			System.out.println();
			System.out.println("*********************************");
			System.out.println("Report");
			System.out.println(".");
			System.out.println(System.getProperty("user.dir")+"\\target\\cucumber-html-reports\\overview-features.html");
			System.out.println(".");
			System.out.println("*********************************");

			
			
			if (suiteProp.getProperty("openReport").equalsIgnoreCase("Yes")) {
				String filePath= System.getProperty("user.dir")+"\\target\\cucumber-html-reports\\overview-features.html";
				File reportFile = new File(filePath);
				Desktop.getDesktop().browse(reportFile.toURI());
			}

		} catch (Exception e) {
			logInfo("ERROR", "Error while executing method " + getMethodName() + " in the class file " + getClassName()
					+ " with error: " + e.toString());
		}
	}
}