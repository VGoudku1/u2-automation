package com.tmobile.CucumberFiles;

import java.util.HashSet;
import java.util.Properties;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.HomePage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class HomePage_SD extends GenericKeywords {

	public static Properties suiteProp = new Properties();
	protected static HashSet<String> failedScenario = new HashSet<>();
	protected static boolean rerun = false;
	HomePage objHomepage = new HomePage();

	@Given("^I select shop option from navigation menu$")
	public void selectMenuItem() throws Exception {
		objHomepage.clickNavigationMenu("SHOP");
		objHomepage.clickNavigationSubMenu("Bring Your Own Device");
	}
	
	@Given("I select {string} sub option from SHOP menu")
	public void selectSubOptions(String subMenuOption) throws Exception {
		objHomepage.clickNavigationMenu("SHOP");
		objHomepage.clickNavigationSubMenu(subMenuOption);
	}
	
	@Given("^I select support option from navigation menu$")
	public void selectSupport() throws Exception {
		objHomepage.clickNavigationMenu("SUPPORT");
	}
	
	@When("^I click on lookup option$")
	public void clickLookupNavigationBar() throws Exception {
		objHomepage.clickLookupOption();

	}
	
	@When("^I click on lookup order option$")
	public void clickLookupOrderOption() throws Exception {
		objHomepage.clickOrderLookupOption();

	}
	
	@When("^I enter order number$")
	public void enterOrderNumberSearch() throws Exception {
		String orderNumber = getTestData().get("OrderNumber");
		objHomepage.enterOrderNumberToLookup(orderNumber);

	}
	
	@When("^Click on search order button$")
	public void clickSearchOrder() throws Exception {
		objHomepage.clickSearchOrderNumberButton();
	}
	
	@When("^Click on searched order result$")
	public void clickSearchedResultsOrder() throws Exception {
		objHomepage.clickSearchedOrderResult();
	}
	
	@When("^Click on bypass option on verification page$")
	public void clickByPassOption() throws Exception {
		objHomepage.clickByPass();
	}

}
