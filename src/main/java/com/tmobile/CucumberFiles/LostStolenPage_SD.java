package com.tmobile.CucumberFiles;

import com.tmobile.pages.LostStolenDevicePage;

import io.cucumber.java.en.When;

public class LostStolenPage_SD {

	LostStolenDevicePage objLostStolenDevicePage = new LostStolenDevicePage();

	@When("^I select yes suspend this line answer$")
	public void clickSuspendYesAns() {
		objLostStolenDevicePage.clickYesSuspendRadioButton();

	}

	@When("^I select yes block my device$")
	public void clickBlockDeviceYesAns() {
		objLostStolenDevicePage.clickYesBlockDeviceRadioButton();
	}
	
	@When("^I Accept suspend terms and conditions$")
	public void acceptSuspendTnC() {
		objLostStolenDevicePage.clickAcceptTnC();
	}
	
	@When("^I submit the suspend request$")
	public void submitSuspend() {
		objLostStolenDevicePage.submitSuspendRequest();
	}
	
	@When("I Click block my IMEI button")
	public void blockIMEIButton() {
		objLostStolenDevicePage.clickBlockMyIMEIButton();
	}

}
