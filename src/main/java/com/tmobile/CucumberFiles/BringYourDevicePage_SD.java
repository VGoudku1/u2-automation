package com.tmobile.CucumberFiles;

import java.util.HashSet;
import java.util.Properties;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.BringYourDevicePage;
import com.tmobile.pages.HomePage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BringYourDevicePage_SD extends GenericKeywords {
	
	public static Properties suiteProp = new Properties();
	protected static HashSet<String> failedScenario = new HashSet<>();
	protected static boolean rerun = false;
	HomePage objHomepage = new HomePage();
	BringYourDevicePage objBringYourDevice = new BringYourDevicePage();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();

	@Given("^I enter ZIP Code$")
	public void enterZipCode() throws Exception {
		String getZip = getTestData().get("ZipCode");
		objBringYourDevice.enterZipCode(getZip);
	}
	
	@Given("^Click on Continue button$")
	public void clickContinue() {
		objBringYourDevice.clickZipCountinueButton();
	}
	
	@When("^I enter IMEI Number$")
	public void enterIEMEINumber() throws Exception {
		String IEMEI = getTestData().get("IMEI Number");
		objBringYourDevice.enterIEMEINumber(IEMEI);	
	}
	
	@When("^Click on check Compatibility$")
	public void checkCompatibility() throws Exception {
		objBringYourDevice.clickCompatibilityButton();		
	}
	
	@Then("^Make sure your device is unlocked$")
	public void verifyValidIEMEI() throws Exception {
//		objBringYourDevice.verifyValidIEMEI();
		objBringYourDevice.clickCloseDialog();
	}
	
	@Then("^I select my phone is unlocked$")
	public void unlockPhoneCheckBox() throws Exception {
		objBringYourDevice.clickUnlockedMyPhoneCheckbox();
	}
	
	@When("^I enter SIM number$")
	public void enterSIMNumber() throws Exception {
		String SIMNumber = getTestData().get("SIM Numbers");
		objBringYourDevice.enterSimNumber(SIMNumber);
	}
	
	@When("^I enter eSIM number$")
	public void enter_eSIMNumber() throws Exception {
		String SIMNumber = getTestData().get("SIM Numbers");
		objBringYourDevice.entereSIMmNumber(SIMNumber);
	}
	
	@When("^Click on Select eSIM card$")
	public void selecteSIMCard() throws Exception {
		objBringYourDevice.selectUseMyeSIMButton();
	}
	
	@When("^Click on Select sim card$")
	public void selectSimCard() throws Exception {
		objBringYourDevice.selectUseMySimButton();
	}
	
	@When("^Click on Select sim you need card$")
	public void selectSimYouNeedCard() throws Exception {
		objBringYourDevice.selectSimYouNeedButton();
	}
	
	@When("^Click continue on port number modal$")
	public void portSimToPrepaid() throws Exception {
		objBringYourDevice.portToPrepaid();
	}
	
	@When("^Click on Choose plan$")
	public void choosePlanButton() throws Exception {
		utilFunction.clickTmoButtonWithText("Choose a plan");
		
	}
	
	

}
