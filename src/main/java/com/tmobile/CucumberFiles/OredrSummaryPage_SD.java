package com.tmobile.CucumberFiles;

import com.tmobile.pages.HomePage;
import com.tmobile.pages.OrderSummaryPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.When;

public class OredrSummaryPage_SD {

	HomePage objHomePage = new HomePage();
	OrderSummaryPage objOrderSummaryPage = new OrderSummaryPage();
	CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	
	@When("^I select Line Details option from T-Mobile$")
	public void openLineDeatils() throws InterruptedException {
		objOrderSummaryPage.clickMyTmobile();
		objOrderSummaryPage.clickMyTmobileSubMenu("Line Details");
		Thread.sleep(2000);
	}
	
	@When("^I select My Profile option from T-Mobile$")
	public void opeTmobileMyProfile() throws InterruptedException {
		objOrderSummaryPage.clickMyTmobile();
		objHomePage.clickNavigationSubMenu("My Profile");
		Thread.sleep(2000);
	}
	
}
