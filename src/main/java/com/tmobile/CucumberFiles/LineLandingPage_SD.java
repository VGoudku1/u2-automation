package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.LineLandingPage;

import io.cucumber.java.en.When;

public class LineLandingPage_SD extends GenericKeywords {
	
	LineLandingPage objLineLandingPage = new LineLandingPage();
	LostStolenPage_SD objLostStolenPage_SD = new LostStolenPage_SD();
	@When("^I click on report lost or stolen link$")
	public void clickReportLostOrStolen() throws Exception {
		objLineLandingPage.reportLost();
	}
	
	@When("I select suspend service or report stolen option")
	public void selectSuspendOptionWithEnv() throws Exception {
		String currentUrl = getUrl();
		if (currentUrl.contains("qlab02")) {
			objLineLandingPage.clickLineDetailsOption("Suspend my service");
		} else {
			objLineLandingPage.reportLost();
//			objLostStolenPage_SD.clickSuspendYesAns();
//			objLostStolenPage_SD.clickBlockDeviceYesAns();
		}
			
	}
	
	@When("I click on line details option {string}")
	public void clickReportLostOrStolen(String lineDetailsOptionName) throws Exception {
		objLineLandingPage.clickLineDetailsOption(lineDetailsOptionName);
	}
	
	@When("I Click on rep actions header with text {string}")
	public void expandHeaders(String expandHeader) {
		objLineLandingPage.expandActionHeader(expandHeader);
	}
	
	@When("I click on change a number heading")
	public void changeNumberHeader() {
		objLineLandingPage.clickChangeNumberButton();
	}
	
	@When("I Click on change plan and services button")
	public void changePlanServices() {
		objLineLandingPage.clickChangePlanSevicesButton();
	}
	
	@When("I Click on add on-demand passes button")
	public void onDemandPassesButton() {
		objLineLandingPage.clickOnDemandPassButton();
	}
	
	@When("I expand data options for demand pass")
	public void expandDataOptions() {
		objLineLandingPage.clickExpandDataHeader();
	}
	
	@When("I select On-Demand pass with text {string}")
	public void selectDemandPass(String demandPassName) {
		objLineLandingPage.selectDemandPassWithName(demandPassName);
	}
	
	@When("I select pass quantity as {int}")
	public void selectPassQuantity(int quantity) {
		objLineLandingPage.selectNoOfPassesQuantity(quantity);
	}
	
	@When("Click on set order date button")
	public void clickSetOrderDateButton() {
		objLineLandingPage.clickSetOrderDate();
	}
	

}
