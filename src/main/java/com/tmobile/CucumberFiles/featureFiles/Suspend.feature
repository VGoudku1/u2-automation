Feature: Suspend and cancell the line

  Background: 
    Given I am in login screen
    When I enter User Details
    And Click on Sign in button
    And Select role as manager care
    And Click continue as role button
    Then user should be on the Rebelion Care Home page

  Scenario: Dollar 15 plan Suspension
    When I click on lookup option
    When I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    Then I click on report lost or stolen link
    Then I select yes suspend this line answer
    And I select yes block my device
    And I Accept suspend terms and conditions
    And I submit the suspend request
    Then I verify order submitted is success
    And Suspended order number is generated

  Scenario: Suspend my device or report stolen
    When I click on lookup option
    When I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    And I select suspend service or report stolen option
    And I select yes suspend this line answer
    And I select yes block my device
    And I Accept suspend terms and conditions
    And I submit the suspend request
    Then I verify order submitted is success
    And Suspended order number is generated
    And Order number is passed to scenario "Resume Cancelled Line"

  Scenario: Cancellation
    When I click on lookup option
    And I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    And I Click on rep actions header with text "Rep actions"
    And I Click on rep actions header with text "Cancel line"
    And I click select the line checkbox
    And I select the cancellation reason as "Billing Issue"
    And I click continue button to cancel line
    And I click continue button with text "Continue"
    Then I verify order is cancelled successfully
    And Cancelled order number is generated

  Scenario: Resume Cancelled Line
    When I click on lookup option
    And I click on lookup order option
    #And I enter order number
    #And Click on search order button
    #And Click on searched order result
    #And Click on bypass option on verification page
    #And I select Line Details option from T-Mobile
    #And I Click on rep actions header with text "Rep actions"
    #And I Click on rep actions header with text "Resume line"
    #And I click select the line checkbox
    #And I click next button
    #--And I Edit the payment
    #--And I enter Credit card deatils
    #--And I enter Credit Card Address
    #--And I click agree Terms and conditions checkbox
    #--And I click continue on payment page
    #--And I click on Accept and place the order
    #--Then I verify order is submitted successfully
    #And Order number is generated

  Scenario: Change SIM
    When I click on lookup option
    When I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    Then I click on line details option "Change SIM"
    And I enter IMEI Number
    And Click on check Compatibility
    And I enter SIM number
    And Click on Select sim card
    And I click on change SIM submit
    Then I verify order submitted is success
    And Change SIM order number is generated
