Feature: Add an On-Demand Passes
As a TMobile user i want to Add an On-Demand Pass

  Background: 
    Given I am in login screen
    When I enter User Details
    And Click on Sign in button
    And Select role as manager care
    And Click continue as role button
    Then user should be on the Rebelion Care Home page

  Scenario: Add an On-Demand Pass
    When I click on lookup option
    When I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    And I Click on add on-demand passes button
    And I expand data options for demand pass
    And I select On-Demand pass with text "On-Demand Domestic On-Network Data / 7 Day / 1GB" 
    And I select pass quantity as 1
    And Click button with aria-lable as "Set order date"
    And Click button with aria-lable as "Add to cart"
    And I click continue button with text "Checkout"
    And I Edit the payment
    And I enter Credit card deatils
    And I enter Credit Card Address
    And I click agree Terms and conditions checkbox
    And I click continue on payment page
    And I click on Accept and place the order
    Then I verify order is submitted successfully
    And Order number is generated