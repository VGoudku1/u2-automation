Feature: Block IMEI numbers
  As a TMobile user i want to block my IMEI numbers which are cancelled

  Background: User logs in into application and navigates to Rebelion homepage
    Given I am in login screen
    When I enter User Details
    And Click on Sign in button
    And Select role as manager care
    And Click continue as role button
    Then user should be on the Rebelion Care Home page

  Scenario: Block IMEI
    When I click on lookup option
    When I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    Then I click on line details option "Restore service"
    And I Click on radio button with text "Keep my IMEI blocked"
    And I click continue button with text " Agree & Submit "

  Scenario: Block My IMEI
    When I click on lookup option
    When I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    And I select suspend service or report stolen option
    And I Click on radio button with text "No, keep this line active but I want to block my IMEI."
    And I Click block my IMEI button
    And I enter IMEI number to block
    And I click on search IMEI button
    And I select block IMEI reason as "Stolen or Undelivered"
    And Click on add IMEI to block button
    Then I verify order submitted is success

  Scenario: Unblock IMEI
  When I click on lookup option
    When I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    When I select support option from navigation menu
    And I Click on support link "Device"
    And I expand the device options header "Unlocking, blocking, and SIMs"
    And I click ubblock sub options link "IMEI block check"
    And I enter IMEI number to block
    And I click on search IMEI button
    And I click remove IMEI from block
    And Click button with aria-lable as "Remove Block"
    Then I verify order submitted is success
