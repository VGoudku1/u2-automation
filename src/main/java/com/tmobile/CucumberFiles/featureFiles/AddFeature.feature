Feature: Add new feature
  As a T-Mobile user i want to change or upgrade the rate plan

  Background: Navigate to Rebelion Care Home Page
    Given I am in login screen
    When I enter User Details
    And Click on Sign in button
    And Select role as manager care
    And Click continue as role button
    Then user should be on the Rebelion Care Home page

  Scenario: Add feature
    When I click on lookup option
    And I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    And I Click on change plan and services button
    And I click continue button with text "Continue to services"
    And I expand the choose service header "Miscellaneous Services"
    And I Click on checkbox with text "Account Takeover Protection"
    And I scroll down the page
    And I click continue button with text " Set order date "
    And I click continue button with text "Add to cart"
    And I click continue button with text "Checkout"
    And I click on Accept and place the order
    Then I verify order is submitted successfully
    And Order number is generated
    
   Scenario: Remove Added feature
    When I click on lookup option
    And I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    And I Click on change plan and services button
    And I click continue button with text "Continue to services"
    #And I expand the choose service header "Miscellaneous Services"
    And I Click on checkbox with text "Account Takeover Protection"
    And I scroll down the page
    And I click continue button with text " Set order date "
    And I click continue button with text "Add to cart"
    And I click continue button with text "Checkout"
    And I click on Accept and place the order without scroll
    Then I verify order is submitted successfully
    And Order number is generated