Feature: Change Rate plan from existing plan
  As a T-Mobile user i want to change or upgrade the rate plan

  Background: Navigate to Rebelion Care Home Page
    Given I am in login screen
    When I enter User Details
    And Click on Sign in button
    And Select role as manager care
    And Click continue as role button
    Then user should be on the Rebelion Care Home page

  Scenario: Change rate plan to $25
    When I click on lookup option
    And I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    And I Click on change plan and services button
    And Select $25 plan from Connect T-Mobile section
    And I click continue button with text "Continue to services"
    And I click continue button with text "Set order date"
    And I click continue button with text "Add to cart"
    And I click continue button with text "Checkout"
    And I Edit the payment
    And I enter Credit card deatils
    And I enter Credit Card Address
    And I click agree Terms and conditions checkbox
    And I click continue on payment page
    And I click on Accept and place the order
    Then I verify order is submitted successfully
    And Order number is generated

  Scenario: Change MSISDN and submit request
    When I click on lookup option
    And I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select Line Details option from T-Mobile
    And I click on change a number heading
    And I click on get new number button
    And I enter zip or area code
    And I click on get this number button
    And I click on submit request button
    Then I verify order is submitted successfully
    And Order number is generated
