Feature: Add new feature
  As a T-Mobile user i want to change or update my profile name

  Background: Navigate to Rebelion Care Home Page
    Given I am in login screen
    When I enter User Details
    And Click on Sign in button
    And Select role as manager care
    And Click continue as role button
    Then user should be on the Rebelion Care Home page

  Scenario: Change First Name
    When I click on lookup option
    And I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select My Profile option from T-Mobile
    And I expands my profile header section "Profile information"
    And I click on edit profile information link "Names"
    And I enter first name
    And I enter last name
    And I click save profile information button
    And I verify changes are saved
    
   Scenario: Change Account Email Address
    When I click on lookup option
    And I click on lookup order option
    And I enter order number
    And Click on search order button
    And Click on searched order result
    And Click on bypass option on verification page
    And I select My Profile option from T-Mobile
    And I expands my profile header section "Profile information"
    And I click on edit profile information link "Email"
    And I enter account email as "testUserEmail@email.com"
    And I retype account email as "testUserEmail@email.com"
    And I click save profile information button
    And I verify changes are saved