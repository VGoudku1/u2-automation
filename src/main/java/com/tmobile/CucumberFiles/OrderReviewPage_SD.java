package com.tmobile.CucumberFiles;

import com.tmobile.commons.GenericKeywords;
import com.tmobile.pages.OrderReviewPage;
import com.tmobile.utils.CommonUtilFunctions;

import io.cucumber.java.en.When;

public class OrderReviewPage_SD extends GenericKeywords{
	
	OrderReviewPage objOrderReviewPage = new OrderReviewPage();
CommonUtilFunctions utilFunction = new CommonUtilFunctions();
	
	@When("^I click on Accept and place the order$")
	public void acceptAndPlaceWithScroll() throws InterruptedException {
		objOrderReviewPage.scrollTermsAndConditions();
		objOrderReviewPage.acceptAndPlaceOrder();
	} 
	
	@When("^I click on Accept and place the order without scroll$")
	public void acceptAndPlace() throws InterruptedException {
		objOrderReviewPage.acceptAndPlaceOrder();
	} 
	

}
